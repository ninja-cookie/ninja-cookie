/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const EventTarget = require("./modules/eventtarget.js");
const portWrapper = require("./modules/portwrapper.js");

// Messages handling
window.background = new EventTarget();
window.tabs = new EventTarget();
window.popups = new EventTarget();
window.options = new EventTarget();

// List of currently opened connections
window.tabPorts = [];
window.popupPorts = [];
window.optionsPort;

// Setup outgoing messages
window.tabs.sendEvent = function(tabId, message) {
  if (window.tabPorts[tabId]) {
    window.tabPorts[tabId].sendEvent(message);
  }
}
window.tabs.sendMessage = function(tabId, message) {
  if (window.tabPorts[tabId]) {
    return window.tabPorts[tabId].sendMessage(message);
  } else {
    return Promise.reject('No port opened for tab ' + tabId);
  }
}

window.popups.sendEvent = function(tabId, message) {
  if (window.popupPorts[tabId]) {
    window.popupPorts[tabId].sendEvent(message);
  }
}
window.popups.sendMessage = function(tabId, message) {
  if (window.popupPorts[tabId]) {
    return window.popupPorts[tabId].sendMessage(message);
  } else {
    return Promise.reject('No popup port opened for tab ' + tabId);
  }
}

window.options.sendEvent = function(message) {
  if (window.optionsPort) {
    window.optionsPort.sendEvent(message);
  }
}
window.options.sendMessage = function(message) {
  if (window.optionsPort) {
    return window.optionsPort.sendMessage(message);
  } else {
    return Promise.reject('No option port opened');
  }
}

// Listen to incoming connections
browser.runtime.onConnect.addListener(port => {
  const wrapper = portWrapper(port);
  if (port.name === 'tab') {
    const tabId = port.sender.tab.id;

    // Cleanup the old port
    const oldPort = window.tabPorts[tabId];
    if (oldPort) {
      oldPort.cleanup();
    }

    // Add new port, disconnect listener and cleanup function
    window.tabPorts[tabId] = wrapper;
    const listener = () => {
      delete window.tabPorts[tabId];
    };
    wrapper.cleanup = () => port.onDisconnect.removeListener(listener);
    port.onDisconnect.addListener(listener);

    // Forward messages to EventTarget
    wrapper.addListener('*', (data, sender, type) =>
      EventTarget.prototype.sendMessage.call(
        window.tabs,
        {type, data},
        sender
      )
    );
  } else if (port.name.startsWith('popup#')) {
    const tabId = +port.name.substring(6);

    // Cleanup the old port
    const oldPort = window.popupPorts[tabId];
    if (oldPort) {
      oldPort.cleanup();
    }

    // Add new port and disconnect listener
    window.popupPorts[tabId] = wrapper;
    const listener = () => delete window.popupPorts[tabId];
    wrapper.cleanup = () => port.onDisconnect.removeListener(listener);
    port.onDisconnect.addListener(listener);

    // Forward messages to EventTarget
    wrapper.addListener('*', (data, sender, type) =>
      EventTarget.prototype.sendMessage.call(
        window.popups,
        {type, data},
        sender
      )
    );
  } else if (port.name === 'options') {
    // Make sure the old port will not remove the new port on disconnect
    const oldPort = window.optionsPort;
    if (oldPort) {
      oldPort.cleanup();
    }

    // Add new port and disconnect listener
    window.optionsPort = wrapper;
    const listener = () => delete window.optionsPort;
    wrapper.cleanup = () => port.onDisconnect.removeListener(listener);
    port.onDisconnect.addListener(listener);

    // Forward messages to EventTarget
    wrapper.addListener('*', (data, sender, type) =>
      EventTarget.prototype.sendMessage.call(
        window.options,
        {type, data},
        sender
      )
    );
  } else {
    console.warn("Unknown connection name", port.name);
    port.disconnect();
  }
});

require("./modules/background/.autoload");
