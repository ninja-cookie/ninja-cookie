/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

$("html").toggleClass(
  "__ninja_cookie_safari",
  navigator.vendor && navigator.vendor.indexOf('Apple') > -1
);

if (window.self === window.top) {
  const portWrapper = require("./modules/portwrapper.js");
  window.background = portWrapper(browser.runtime.connect({
    name: 'tab',
  }));
}

require("./modules/content_script/.autoload");
