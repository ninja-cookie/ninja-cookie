/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

/**
 * An abstract JSON object describing an action to be performed by Ninja Cookie.
 *
 * @typedef {Object} Action
 * @property {string} type - The type of action being defined.
 * @property {boolean} [optional=false] - Whether the action is optional.
 *   If an action is optional, the following actions will be executed regardless
 *   of the returned result of this action.
 */

/**
 * A JSON object describing an error that occured
 * during an {@link Action} execution.
 *
 * @typedef {Object} ActionError
 * @property {string} error - The type of error that occured.
 * @property {string} [reason] - A message describing the error that occured.
 */

const Actions = {
  _waitForStillness: async function(stillness) {
    // Wait until some time have passed without any DOM modification
    console.log("Wait for stillness", stillness);
    await new Promise(resolve => {
      const callback = () => {
        observer.disconnect();
        resolve();
      }
      let timer = setTimeout(callback, stillness);
      const observer = new MutationObserver(() => {
        clearTimeout(timer);
        timer = setTimeout(callback, stillness);
      });
      observer.observe(document, {
        subtree: true,
        childList: true,
        attributes: true,
        characterData: true,
      });
    });
  },

  _waitForReadyState: async function (readyState) {
    // Wait until a document state is reached
    console.log("Wait for readyState", readyState);
    await new Promise(resolve => {
      switch (document.readyState) {
        case 'complete':
          return resolve();
        case 'interactive':
          if (readyState !== 'complete')
            return resolve();
        case 'loading':
        default:
          if (readyState === document.readyState)
            return resolve();
          else {
            const handler = () => {
              if (document.readyState === readyState) {
                document.removeEventListener('readystatechange', handler);
                resolve();
              }
            };
            return document.addEventListener('readystatechange', handler);
          }
      }
    });
  },

  _reserved : ['register', 'execute'],
  _registered : {},
  register: function(names, fn, attributes = {}) {
    // Wrap function to handle attributes
    const wrappedFn = async function() {
      if (!!attributes.stillness) {
        await Actions._waitForStillness(!!attributes.stillness);
      }
      if (!!attributes.readyState) {
        await Actions._waitForReadyState(!!attributes.readyState);
      }

      return await fn.apply(this, arguments);
    }
    wrappedFn.recursive = !!attributes.recursive;

    if (typeof names === "string")
      names = [names];
    names.forEach(name => {
      if (name.startsWith('_') || Actions._reserved.includes(name)) {
        console.warn('Action', name, 'is reserved. Ignored');
      } else {
        Actions._registered[name] = Actions[name] = wrappedFn;
      }
    });
  },

  _lastName: 'anonymous',
  _lastMetadata: {},
  _lastOptions: {},
  execute: async function({
    name = Actions._lastName,
    metadata = Actions._lastMetadata,
    action = null,
    actions = [],
    selector = 'html',
    options = Actions._lastOptions,
  }) {
    Actions._lastName = name;
    Actions._lastMetadata = metadata;
    Actions._lastOptions = options;

    if (actions.length === 0 && action) {
      actions = action;
    }

    if (!Array.isArray(actions)) {
      actions = [actions];
    }

    // Remove option until the last action execution
    const signalLastAction = !!options.signalLastAction;
    delete options.signalLastAction;

    for (let i in actions) {
      const action = actions[i];

      // Send message if this is the last action to be executed in a frame
      // and the result might never be sent because of rapid unloading.
      if (signalLastAction && +i === actions.length - 1) {
        if (!Actions._registered[action.type].recursive) {
          await browser.runtime.sendMessage(window.opener || window.parent, {
            type: 'action.execute_last',
          });
        } else {
          options.signalLastAction = signalLastAction;
        }
      }

      try {
        selector = await Actions._execute({
          name,
          metadata,
          action,
          selector,
          options,
        });
      } catch (err) {
        // Log unexpected errors
        if (err instanceof Error) {
          console.error(err);
        }
        // An action can be expected to fail
        if (!action.optional) {
          throw { name: name, action: action, ...err };
        }
      }
    }
    return selector;
  },

  _execute: async function({
    name,
    metadata,
    action,
    selector,
    options,
  }) {
    if (action.selector) {
      if (typeof action.selector === "object" && !action.selector.root) {
        action.selector.root = selector;
      }
      selector = action.selector;
    }

    if (options.log) {
      console.debug("Executing", name, action, selector);
    }

    if (action.type in Actions._registered) {
      // Execute action
      const result = await Actions._registered[action.type]({
        action,
        selector,
        options,
        name,
        metadata,
      });
      if (typeof result !== "undefined")
        return result;
      else
        return selector;
    }

    console.warn("Unknown action type", action.type);
    throw {
      error: "unknown_action",
      // @desc An unknown action type is executed
      // @param type=foo
      reason: browser.i18n.getMessage('unknownAction', action.type),
    };
  },
};

module.exports = Actions;

require('./actions/.autoload');
