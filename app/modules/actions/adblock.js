/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");
const Sentinel = require('sentinel-js');

(function() {
  let status = 'disabled';
  let callbacks = {};
  function enableNewFeaturesHandling(listId, cb) {
    if (status === 'disabled') {
      status = 'enabled';
    }
    callbacks[listId] = cb;
    handleNewFeatures({
      ids: Array.from(knownIds),
      classes: Array.from(knownClasses),
      hrefs: Array.from(knownHrefs),
      listId: listId,
    });
  }
  function disableNewFeaturesHandling(listId) {
    delete callbacks[listId];
    if (!callbacks.length) {
      status = 'disabled';
    }
  }

  async function handleNewFeatures({ids, classes, hrefs, listId = null}) {
    // Check new features only when enabled
    if (status === 'disabled') return;

    const selectors = await window.background.sendMessage({
      type: 'action.adblock',
      data: {
        ids: ids,
        classes: classes,
        hrefs: hrefs,
        listId: listId,
      },
    });
    Object.entries(selectors).map(([listId, selectors]) => {
      if (!selectors.length) return;
      const selected = $(selectors.join(','));
      if (!selected.length) return;
      callbacks[listId](selected);
    });
  }

  /* From @cliqz/adblocker webextension example */
  const knownIds = new Set();
  const knownClasses = new Set();
  const knownHrefs = new Set();

  function getNewFeatures({ids, classes, hrefs}) {
    const newIds = [];
    const newClasses = [];
    const newHrefs = [];

    // Update ids
    for (let i = 0; i < ids.length; i += 1) {
      const id = ids[i];
      if (knownIds.has(id) === false) {
        newIds.push(id);
        knownIds.add(id);
      }
    }

    for (let i = 0; i < classes.length; i += 1) {
      const cls = classes[i];
      if (knownClasses.has(cls) === false) {
        newClasses.push(cls);
        knownClasses.add(cls);
      }
    }

    for (let i = 0; i < hrefs.length; i += 1) {
      const href = hrefs[i];
      if (knownHrefs.has(href) === false) {
        newHrefs.push(href);
        knownHrefs.add(href);
      }
    }

    if (newIds.length !== 0 || newClasses.length !== 0 || newHrefs.length !== 0) {
      return({
        classes: newClasses,
        hrefs: newHrefs,
        ids: newIds,
      });
    }

    return false;
  }

  function extractFeaturesFromDOM(elements) {
    const ignoredTags = new Set(['br', 'head', 'link', 'meta', 'script', 'style']);
    const classes = new Set();
    const hrefs = new Set();
    const ids = new Set();

    for (let i = 0; i < elements.length; i += 1) {
      const element = elements[i];

      if (element.nodeType !== 1 /* Node.ELEMENT_NODE */) {
        continue;
      }

      if (element.localName !== undefined && ignoredTags.has(element.localName)) {
        continue;
      }

      // Update ids
      const id = element.id;
      if (id) {
        ids.add(id);
      }

      // Update classes
      const classList = element.classList;
      if (classList) {
        for (let j = 0; j < classList.length; j += 1) {
          classes.add(classList[j]);
        }
      }

      // Update href
      const href = element.href;
      if (typeof href === 'string') {
        hrefs.add(href);
      }
    }

    return {
      classes: Array.from(classes),
      hrefs: Array.from(hrefs),
      ids: Array.from(ids),
    };
  }

  function getDOMElementsFromMutations(mutations) {
    // Accumulate all nodes which were updated in `nodes`
    const nodes = [];
    for (let i = 0; i < mutations.length; i += 1) {
      const mutation = mutations[i];
      if (mutation.type === 'attributes') {
        nodes.push(mutation.target);
      } else if (mutation.type === 'childList') {
        const addedNodes = mutation.addedNodes;
        for (let j = 0; j < addedNodes.length; j += 1) {
          const addedNode = addedNodes[j];
          nodes.push(addedNode);

          if (addedNode.querySelectorAll !== undefined) {
            const children = addedNode.querySelectorAll('[id],[class],[href]');
            for (let k = 0; k < children.length; k += 1) {
              nodes.push(children[k]);
            }
          }
        }
      }
    }
    return nodes;
  }

  const observer = new MutationObserver(async mutations => {
    const newFeatures = getNewFeatures(
      extractFeaturesFromDOM(
        getDOMElementsFromMutations(mutations)
      )
    );

    if (newFeatures) {
      await handleNewFeatures(newFeatures);
    }
  });
  observer.observe(window.document.documentElement, {
    attributeFilter: ['class', 'id', 'href'],
    attributes: true,
    childList: true,
    subtree: true,
  });

  // Run once to retrieve initially available ids/classes/hrefs
  getNewFeatures(
    extractFeaturesFromDOM(
      Array.from(window.document.querySelectorAll('[id],[class],[href]'))
    )
  );

  const domLoaded = new Promise(r => $(r));

  /* When adblock action is called, retrieve selectors for already known
   * ids/classes/hrefs and setup onNewFeatures callback.
   *
   * `selector` contains all the non-basic adblock selectors.
   */
  Actions.register(['adblock'], async ({action, selector}) => {
    // Format input
    if (!Array.isArray(selector)) {
      selector = [selector];
    }

    // Get timeout value
    let timeout = typeof action.timeout === "number" ? action.timeout : 30000;
    if (timeout === 0) {
      throw {
        // @desc No DOM elements matching the provided CSS selectors were found
        reason: browser.i18n.getMessage('checkNoMatch'),
      };
    }

    // Wait for DOM to be loaded before inserting stylesheet in header
    await domLoaded;

    // Wait for element to appear or timeout
    return new Promise((resolve, reject) => {
      let timer;
      if (Number.isFinite(timeout)) {
        timer = setTimeout(() => {
          clearTimeout(timer);
          reject({
            reason: browser.i18n.getMessage('checkTimeout'),
          });
        }, timeout);
      }

      selector.forEach(selector => Sentinel.on(selector, selected => {
        clearTimeout(timer);
        selected = $(selected);
        selected.selector = selector;
        resolve(selected);
      }));
      enableNewFeaturesHandling(action.listId, selected => {
        clearTimeout(timer);
        resolve(selected);
      });
    }).finally(() => {
      selector.map(Sentinel.off);
      disableNewFeaturesHandling(action.listId);
    });
  });

})();
