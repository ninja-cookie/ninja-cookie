/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

(() => {
  // Observers
  let observers = [];

  function select(selector) {
    if (selector instanceof $) {
      return selector;
    }

    if (typeof selector === "string") {
      return $(selector);
    }

    let root;
    if (selector.root) {
      root = select(selector.root);
    } else {
      root = $(document);
    }

    return root.map((i, e) => {
      if (e.shadowRoot) {
        return [...e.shadowRoot.querySelectorAll(selector.target)];
      } else {
        return [...e.find(selector.target)];
      }
    });
  }

  /**
   * @summary Check/wait for DOM element(s).
   *
   * @typedef CheckAction
   * @type {Action}
   * @extends {Action}
   * @property {string} type - Must be one of `check`, `verify`.
   * @property {string} selector - A DOM selector for the expected element(s).
   * @property {string} [options] - Must be one of `attributes`, `class`.
   *   If defined, the underling {@link MutationObserver} will check for
   *   attributes mutations or class mutations respectively. Otherwise,
   *   it will check for new DOM elements only.
   * @property {number} [timeout=30000] - The time, in milliseconds,
   *   to wait for the expected element(s) to appear.
   */
  Actions.register(['check', 'verify'], async ({action, selector}) => {
    // Format input
    if (!Array.isArray(selector)) {
      selector = [selector];
    }

    // Check if element already exists
    for (let i = 0; i < selector.length; ++i) {
      const selected = select(selector[i]);
      if (selected.length) {
        selected.selector = selector[i];
        return selected;
      }
    }

    // Cleanup selectors and timer when done
    const cleanup = function(observer, selector) {
      clearTimeout(selector.timer);
      observer.selectors = observer.selectors.filter(s => s !== selector);
      if (!observer.selectors.length) {
        observer.disconnect();
        observers = observers.filter(o => o !== observer);
      }
    };

    // Handle observer match
    const match = function() {
      const observer = this;
      observer.selectors.forEach(selector => {
        selector.values.forEach(value => {
          const selected = select(value);
          if (selected.length) {
            cleanup(observer, selector);
            selected.selector = value;
            selector.callback(selected);
          }
        });
      })
    };

    // Get observer options
    let options = {};
    switch (action.options) {
      case "attributes":
        options = { subtree: true, attributes: true };
        break;
      case "class":
        options = { subtree: true, attributes: true, attributeFilter: ["class"] };
        break;
      default:
        options = { subtree: true, childList: true };
        break;
    }

    // Try to find re-usable observer
    let observer = null;
    for (let i in observers) {
      const obs = observers[i];
      if (JSON.stringify(obs.options) == JSON.stringify(options)) {
        observer = obs;
        break;
      }
    }

    // Create observer
    if (!observer) {
      observer = new MutationObserver(match);
      observer.observe(document, options);
      observer.options = options;
      observer.selectors = [];
      observers.push(observer);
    }


    // Wait for match or timeout
    return new Promise((resolve, reject) => {
      // Get timeout value
      let timeout = typeof action.timeout === "number" ? action.timeout : 30000;
      if (timeout === 0) {
        throw {
          // @desc No DOM elements matching the provided CSS selectors were found
          reason: browser.i18n.getMessage('checkNoMatch'),
        };
      }

      // Create selector
      let queuedSelector = {
        values: selector,
        callback: resolve,
        timer: null,
      };

      // Add timeout
      queuedSelector.timer = setTimeout(() => {
        cleanup(observer, queuedSelector);
        reject({
          reason: browser.i18n.getMessage('checkTimeout'),
        });
      }, timeout);

      // Add selectors to observer
      observer.selectors.push(queuedSelector);
    });
  });
})();
