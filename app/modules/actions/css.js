/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Change CSS properties of the selected element(s).
 *
 * @typedef CssAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one fo `css`, `style`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 * @property {object} properties - An object of CSS property/value pairs to set.
 */
Actions.register(['css', 'style'], async ({action, selector}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});
  selected.css(action.properties);
  return selected;
});
