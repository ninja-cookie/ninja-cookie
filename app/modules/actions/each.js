/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

/**
 * @summary Execute one or more {@link Action}(s)
 *   for each of the selected element(s).
 *
 * @typedef EachAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one of `loop`, `for`, `each`, `forEach`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 * @property {(Action|Action[])} action - One or more {@link Action}s to be
 *   executed for each of the selected elements.
 */
Actions.register(['loop', 'for', 'each', 'forEach'], async ({action, selector}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});

  for (let i = 0; i < selected.length; ++i) {
    await Actions.execute({
      actions: action.action,
      selector: $(selected.get(i)),
    });
  }

  return selected;
}, {recursive: true});
