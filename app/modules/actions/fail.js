/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Stop execution and set rule status.
 *
 * @typedef FailAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `fail`, `error`, `abort`.
 * @property {string} [error=failed] - The type of error that occured.
 * @property {string} [reason] - A message describing the error that occured.
 */
Actions.register(['fail', 'error', 'abort'], ({action}) => {
  throw {
    name: action.error ? action.error : 'failed',
    reason: action.reason ? action.reason : undefined,
  };
});
