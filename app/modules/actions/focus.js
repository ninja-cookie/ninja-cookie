/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Focus an external window.
 *
 * @description
 * **WARNING**: This action can only be used from an external window.
 *
 * @typedef FocusAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be `focus`.
 */
Actions.register(['focus'], async () => {
  await window.background.sendMessage({
    type: 'action.focus',
  });
});
