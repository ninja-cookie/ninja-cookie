/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

/**
 * @summary Execute one or more {@link Action}(s) in an internal frame.
 *
 * @typedef InternalAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one of `internal`, `iframe`, `frame`.
 * @property {string} selector - A DOM selector for the internal frame(s)
 *   on which to execute the provided action(s).
 * @property {(Action|Action[])} action - One or more {@link Action}s to be
 *   executed in the internal frame.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected internal frame to appear.
 * @property {number} [executionTimeout=10000] - The time, in milliseconds,
 *   the internal frame has to initialize and execute the provided action(s).
 *
 *   **NOTE**: For backward compatibility, if `executionTimeout` isn't
 *   provided, the `timeout` value will be used.
 * @property {boolean} [unloading=false] - Whether the internal frame should
 *   wait for the page to unload after the action(s) have been executed.
 * @property {boolean} [mightNotReturn=false] - Consider that when the last
 *   action is started, the rule is successful. Useful when a frame is unloading
 *   during action execution.
 */
Actions.register(['internal', 'iframe', 'frame'], async ({action, selector, options, name, metadata}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});

  // For backward compatibility
  if (typeof action.executionTimeout !== "number") {
    action.executionTimeout = action.timeout;
  }

  try {
    // Send execute message to iframes
    return await Promise.all(
      selected.map(async (idx, iframe) => {
        return new Promise((resolve, reject) => {
          // Set execution timeout
          const timer = setTimeout(() => {
            clearTimeout(timer);
            reject({
              reason: browser.i18n.getMessage('internalTimeout'),
            });
          }, action.executionTimeout || 10000);

          let done = false;

          // Listen to message from children before executing the last action
          // which may not return as it will lead to frame unloading
          const messageHandler = ({type}) => {
            if (type !== "action.execute_last") return;
            done = true;

            browser.runtime.onMessageInternal.removeListener(messageHandler);
          }
          if (!!action.mightNotReturn) {
            options.signalLastAction = true;
            browser.runtime.onMessageInternal.addListener(messageHandler);
          }

          // Start action execution
          browser.runtime.onUnloadInternal.addListener(() => {
            clearTimeout(timer);
            return done ? resolve(iframe) : reject({
              reason: browser.i18n.getMessage('internalFrameUnloaded'),
            });
          });
          browser.runtime.sendMessage(iframe, {type: 'action.execute', data: {
            actions: action.action,
            options: options,
            name: name + ' [' + action.type + ']',
            metadata: metadata,
          }}).then(result => {
            if (!!action.unloading) {
              // Wait for unloading
              done = true;
            } else {
              // Return result
              resolve(result);
            }
          }, reject).finally(() => {
            browser.runtime.onMessageInternal.removeListener(messageHandler);
            delete options.signalLastAction;
          });
        });
      })
    );
  } catch (err) {
    if (options.log) {
      console.debug('iframe', selected, 'error', err);
    }
    throw err;
  }
}, {recursive: true});
