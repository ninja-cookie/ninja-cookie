/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Reload the current page.
 *
 * @typedef ReloadAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `reload`, `redirect` or `location`.
 * @property {number} [timeout=0] - The redirection delay, in milliseconds.
 * @property {string} [location] - The URL to redirect to. If not provided,
 *   the page will only be reloaded.
 */
Actions.register(['reload', 'redirect', 'location'], ({action}) => {
  const timeout = typeof action.timeout === "number" ? action.timeout : 0;

  setTimeout(() => {
    if (action.location)
      window.location.replace(action.location);
    else
      window.location.reload();
  }, timeout);
});
