/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");
const Config = require("../config.autoload");

/**
 * @summary Evaluate a script content in the page context.
 *
 * @description
 * **WARNING**: This type of action is a gateway for security breaches and
 * should be avoided when possible. The user will be asked for authorization
 * to execute such scripts, unless the rule comes from one of the default
 * Ninja Cookie lists.
 *
 * @typedef ScriptAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `script`, `eval`, `evaluate`.
 * @property {number} [timeout=20000] - The time, in milliseconds,
 *   to wait for the script to be avaluated.
 * @property {string} content - The script content to evaluate.
 *
 *   It is evaluated as follow :
 *   ```
 *   (async () => ' + action.content + ')()
 *   ````
 *
 *   The content can be a simple expression (eg. `2+2`), can be asynchronous
 *   (eg. `new Promise(r => setTimeout(() => r(2+2), 1000))`) and can use
 *   internal variables (eg. `{let foo = 2; return foo + foo; }`).
 */
Actions.register(['script', 'eval', 'evaluate'], async ({action, options, metadata}) => {
  // Check if the list this rule is coming from trusted
  // If a list is not trusted, a prompt is show asking for the user consent.
  // This is necessary to avoid a big security issue.
  const list = Object.values(metadata)[0].list;

  let trusted = list.trusted;
  if (!trusted && !options.trustAll) {
    const authorized = confirm(
      // @desc The message displayed before running script from untrusted origin
      // @param content=alert('You have been p0wned')
      // @param url=https://127.0.0.1:8080/cmp.json
      browser.i18n.getMessage('scriptRunUntrusted', [
        action.content,
        list.url,
      ])
    );
    if (!authorized) {
      throw {
        reason: browser.i18n.getMessage('scriptUnauthorized'),
      };
    }
  }

  const timeout = typeof action.timeout === "number" ? action.timeout : 20000;
  const script = $(
    '<script id="__ninja_cookie_script">' +
      '(async function() {' +
      '  window.dispatchEvent(new CustomEvent("__ninja_cookie_script", { detail:' +
      '    await (async () => ' + action.content + ')().then(' +
      '      result => ({ resolve: result }),' +
      '      error => ({ reject: error }),' +
      '    )' +
      '  }))' +
      '})();' +
    '</script>'
  );

  return await new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      clearTimeout(timer);
      reject({
        reason: browser.i18n.getMessage('scriptTimeout'),
      });
    }, timeout);
    window.addEventListener("__ninja_cookie_script", e => {
      clearTimeout(timer);
      const result = e.detail;
      if ('resolve' in result) {
        resolve(result.resolve);
      } else {
        if (options.log) {
          console.error(result.reject);
        }
        reject({
          reason: browser.i18n.getMessage('scriptError', [
            // TODO: format error result.reject
          ]),
        });
      }
    }, { once: true });
    $(document.body).append(script);
  }).finally(
    () => script.remove()
  );
});
