/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Actions = require("../actions.js");

/**
 * @summary Show the selected element(s).
 *
 * @typedef ShowAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be `show`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 */
Actions.register(['show'], async ({action, selector, options}) => {
  if (typeof action.timeout !== "number") action.timeout = 0;
  const selected = await Actions.check({action, selector});
  if (options.show === "transparent") {
    selected.css('opacity', '');
  } else if (!options.show) {
    selected.each((idx, elem) => $(elem).attr(
      'style', ($(elem).attr('old-style') || "") + ';overflow:unset!important'
    ).removeAttr('old-style'));
  }
  return selected;
});
