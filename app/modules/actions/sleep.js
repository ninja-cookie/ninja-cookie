/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Sleep for some time.
 *
 * @typedef SleepAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be one of `sleep`, `timeout`, `wait`.
 * @property {number} [timeout=500] - The time, in milliseconds, to sleep for.
 */
Actions.register(['sleep', 'timeout', 'wait'], async ({action, options}) => {
  let timeout = typeof action.timeout === "number" ? action.timeout : 500;
  return new Promise(resolve => {
    const timer = setTimeout(() => {
      clearTimeout(timer);
      resolve();
    }, timeout);
  });
});
