/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Check cookie/local storage value.
 *
 * @typedef StorageAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - The storage to check the value from.
 *   Must be one of `cookie`, `localStorage`.
 * @property {string} name - The key to retrieve from the storage
 * @property {boolean} [missing=false] - Whether the value should be missing.
 *   If set to `true`, the value **MUST** be missing.
 * @property {boolean} [empty=false] - Whether the value should be empty.
 *   If set to `true`, the value **MUST** be empty.
 * @property {string} [regex] - The regular expression the value should be
 *  matched against. If not provided, the value will be accepted.
 */
Actions.register(['cookie', 'localStorage'], ({action}) => {
  // Retrieve values
  let found;
  if (action.type === "cookie") {
    found = document.cookie.split('; ').filter(
      v => v.startsWith(action.name + '=')
    ).map(
      v => decodeURIComponent(v.substring(action.name.length + 1))
    );
  } else {
    found = [localStorage.getItem(action.name)].filter(i => !!i);
  }

  // Check if a value is expected
  if (action.missing) {
    if (found.length) {
      throw {
        reason: browser.i18n.getMessage('storageNotMissing'),
      };
    }
  }

  // Check if a value should be empty
  const value = found.length ? found[0] : "";
  if (action.empty) {
    if (value.length) {
      throw {
        reason: browser.i18n.getMessage('storageNotEmpty'),
      };
    }
  }

  // Check if value is correct
  if (!value.match(new RegExp(action.regex))) {
    throw {
      reason: browser.i18n.getMessage('storageNoMatch'),
    };
  }
});
