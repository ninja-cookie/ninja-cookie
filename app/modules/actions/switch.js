/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Check/wait for multiple element(s) and
 *   execute one or more associated {@link Action}(s).
 *
 * @typedef SwitchAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be `switch`.
 * @property {Object[]} case - An array of element(s) to check/wait for
 *   and their associated {@link Action}(s).
 * @property {string} case[].selector - A DOM selector
 *   for the expected element(s).
 * @property {(Action|Action[])} case[].action - One or more {@link Action}s
 *   to be executed if the associated selector is found.
 * @property {(Action|Action[])} [default] - One or more {@link Action}s to be
 *   executed if no element has been matched. If there is no matched and
 *   the `default` parameter isn't provided, an error will occur.
 * @property {string} [options] - Must be one of `attributes`, `class`.
 *   If defined, the underling {@link MutationObserver} will check for
 *   attributes mutations or class mutations respectively. Otherwise,
 *   it will check for new DOM elements only.
 * @property {number} [timeout=30000] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 */
Actions.register('switch', async ({action}) => {
  let kase;
  try {
    const result = await Actions.check({
      action,
      selector: action.case.map(c => c.selector)
    });
    kase = action.case.find(c => c.selector === result.selector);
  } catch (err) {
    if (action.default) {
      return await Actions.execute({ action: action.default });
    } else {
      throw err;
    }
  }
  return Actions.execute({ actions: kase.action, selector: kase.selector });
}, {recursive: true});
