/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Check/wait for DOM element(s) and
 *  execute one or more {@link Action}(s) based on result.
 *
 * @typedef TestAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one of `test`, `if`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 * @property {(Action|Action[])} true - One or more {@link Action}s to be
 *   executed if element(s) are found.
 * @property {(Action|Action[])} false - One or more {@link Action}s to be
 *   executed if element(s) are *not* found.
 */
Actions.register(['test', 'if'], async ({action, selector}) => {
  action.timeout = typeof action.timeout === "number" ? action.timeout : 0;
  action.true = typeof action.true === "object" ? action.true : [];
  action.false = typeof action.false === "object" ? action.false : [];

  let selected;
  try {
    selected = await Actions.check({action, selector});
  } catch {
    // Discard error
  }

  return await Actions.execute({ action: selected ? action.true : action.false, selector: selected });
}, {recursive: true});
