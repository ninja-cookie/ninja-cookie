/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Check if current URL matches a regex.
 *
 * @typedef UrlAction
 * @type {Action}
 * @extends {Action}
 * @property {string} type - Must be `url`.
 * @property {(string|string[])} regex - One or more regular expressions for
 *   the URL to be matched against.
 */
Actions.register(['url'], ({action}) => {
  let regexes = action.regex;
  if (typeof regexes === "string") {
    regexes = [regexes];
  }
  if (!Array.isArray(regexes)) {
    throw {
      reason: browser.i18n.getMessage('urlBadParameter', [
        'regex'
      ]),
    };
  }
  for (let i in regexes) {
    if (window.location.hostname.match(new RegExp(regexes[i])))
      return;
  }
  throw {
    reason: browser.i18n.getMessage('urlNoMatch'),
  };
});
