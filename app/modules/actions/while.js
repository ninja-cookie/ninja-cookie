/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require("../actions.js");

/**
 * @summary Execute one or more {@link Action}(s)
 *   as long as the selected element(s) is/are found.
 *
 * @typedef WhileAction
 * @type {CheckAction}
 * @extends {CheckAction}
 * @property {string} type - Must be one of `loop`, `for`, `each`, `forEach`.
 * @property {number} [timeout=0] - The time, in milliseconds,
 *   to wait for the expected element(s) to appear.
 * @property {(Action|Action[])} action - One or more {@link Action}s to be
 *   executed each time.
 * @property {boolean} [once=false] - Whether the {@link Action}(s) should be
 *   executed at least once.
 */
Actions.register(['while'], async ({action, selector}) => {
  action.timeout = typeof action.timeout === "number" ? action.timeout : 0;

  let once = false;
  while (true) {
    let selected;
    try {
      selected = await Actions.check({action, selector});
      once = true;
    } catch {
      break;
    }

    await Actions.execute({ action: action.action, selector: selected });
  }
  if (!!action.once && !once)
    throw {
      // @desc No DOM elements matching the provided CSS selectors were found
      reason: browser.i18n.getMessage('checkNoMatch'),
    };
}, {recursive: true});
