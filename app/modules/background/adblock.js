/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Rules = require("../rules.js");
const psl = require('psl');

window.tabs.addListener('action.adblock', async (data, sender) => {
  // Wait for rules to be loaded
  const engines = (await Rules.getData()).blockerEngines;

  const listIds = data.listId ? [data.listId] : Object.keys(engines);
  const hostname = (new URL(sender.tab.url)).hostname;
  const domain = psl.parse(hostname).domain;

  const result = Object.fromEntries(listIds.map(
    listId => ([
      listId,
      // FIXME: Parsing generated CSS is not ideal...
      [...new Set(engines[listId].getCosmeticsFilters({
        hostname: hostname,
        domain: domain,

        ids: data.ids,
        classes: data.classes,
        hrefs: data.hrefs,

        getBaseRules: false,
        getInjectionRules: false,
        getRulesFromHostname: false,
        getRulesFromDOM: true,
      }).styles.replaceAll(
        ' { display: none !important; }', ''
      ).replaceAll(
        ',', ''
      ).split('\n').filter(l => !!l))]
    ])
  ));
  return result;
});
