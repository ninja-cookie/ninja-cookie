/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const State = require("../state.js");

// Wait for basic states
require('./state.js');

(function() {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Define external state
  State.define("external", {
    // @desc The external status text
    "status": browser.i18n.getMessage('externalStatus'),
    // @desc The external status description
    "desc": browser.i18n.getMessage('externalStatusDesc'),
    "sticker": {
      "src": "/icons/external-32.png",
      // The external status alternative text
      "alt": browser.i18n.getMessage('externalStatusAlt'),
    },
    "icons": {
      "16": "icons/external-16.png",
      "24": "icons/external-24.png",
      "32": "icons/external-32.png"
    },
    "report": {
      "category": 0,
      // @desc The default External message displayed in the report form
      // @param url=https://google.com/
      // @param version=0.2.4
      // @param userAgent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
      // @param matched=[]
      "messageId": getMessage('externalReportMessage'),
    },
  }, (b, a, old) => old ? old : 'external');


  // Wait until the target page loading is complete
  function runExternalActionWhenReady(externalTabId, state) {
    // Check if external execution already started
    if (state.metadata.status === 'running')
      return;

    // External tab is loading
    state.metadata.status = 'loading';

    // Wait for tab to be fully loaded
    const updateHandler = ({tabId, newState}) => {
      if (tabId !== externalTabId) return;

      if (!newState.metadata.initialized) return;
      window.background.removeListener('state.set', updateHandler);

      // Only run action once
      if (newState.metadata.status === 'running') return;

      runExternalAction(tabId, newState);
    };

    // Add update handler
    window.background.addListener('state.set', updateHandler);
  }

  function runExternalAction(tabId, state) {
    // External action is now running
    state.metadata.status = 'running';

    // Clear window loading timeout
    clearTimeout(state.metadata.timer);
    delete state.metadata.timer;

    // Get external action data
    const data = state.metadata.data;

    // Prepare execution
    let doneHandler, failedHandler, unloadHandler;
    if (data.unloading) {
      doneHandler = () => {
        state.metadata.done = true;
      };
      failedHandler = err => {
        state.metadata.reject(err);
      };
      unloadHandler = () => {
        if (state.metadata.done) {
          state.metadata.resolve();
        } else {
          state.metadata.reject({
            reason: browser.i18n.getMessage('externalWindowUnloaded'),
          });
        }
      };
    } else {
      doneHandler = () => {
        state.metadata.done = true;
        state.metadata.resolve();
      };
      failedHandler = err => {
        state.metadata.reject(err);
      };
      unloadHandler = () => {
        if (!state.metadata.done) {
          state.metadata.reject({
            reason: browser.i18n.getMessage('externalWindowUnloaded'),
          });
        }
      };
    }

    // Check for URL change.
    // At this point all redirections should be done.
    // The next URL update will be a tab unload.
    const eventHandler = data => {
      if (tabId !== data.tabId)
        return;

      const {oldState, newState} = data;
      if (newState && oldState.metadata.url === newState.metadata.url)
        return;

      window.background.removeListeners([
        'state.set',
        'state.remove',
      ], eventHandler);
      unloadHandler();
    }
    window.background.addListeners([
      'state.set',
      'state.remove',
    ], eventHandler);

    // Set timeout for action execution
    state.metadata.timer = setTimeout(() => {
      state.metadata.reject({
        reason: browser.i18n.getMessage('externalTimeout'),
      });
    }, data.executionTimeout || 10000);

    // Start action execution
    window.tabs.sendMessage(
      tabId,
      {type: 'action.execute', data}
    ).then(doneHandler, failedHandler);
  }

  window.tabs.addListeners([
    'action.window',
    'action.external',
  ], async (data, sender) => {
    let winId;
    let tabId;
    // Firefox for Android cannot create windows
    if (browser.windows && browser.windows.create) {
      // Create a new window with one tab
      const win = await browser.windows.create({
        incognito: sender.tab.incognito,
        state: data.options.show ? "maximized" : "minimized",
      });
      if (data.options.show) {
        await browser.windows.update(win.id, { focused: true });
      }
      winId = win.id;
      tabId = win.tabs[0].id;
    } else {
      // Create a new tab
      const tab = await browser.tabs.create({
        active: !!data.options.show,
      });
      tabId = tab.id;
    }

    // Update sender metadata
    const senderState = State.get(sender.tab.id);
    await State.update(sender.tab.id, senderState.name, {winId, tabId});

    // Give the tab some time to open
    await new Promise(r => setTimeout(r, 500));

    // Set tab state to 'external' and setup callbacks
    return await new Promise((resolve, reject) => {
      const state = State.update(tabId, 'external', {
        data,
        refererId: sender.tab.id,
        resolve,
        reject,
        timer: setTimeout(
          () => reject({
            reason: browser.i18n.getMessage('externalTimeout'),
          }),
          data.executionTimeout || 10000
        ),
      }, sender);
      browser.tabs.update(tabId, {
        url: data.url,
      });
    }).finally(async () => {
      const state = State.get(tabId);
      if (state.metadata.timer) {
        // Clear execution timeout
        clearTimeout(state.metadata.timer);
        delete state.metadata.timer;
      }

      // Start grace period
      if (data.gracePeriod) {
        await new Promise(r => setTimeout(r, data.gracePeriod));
      }

      // Close external window/tab
      if (winId) {
        try {
          await browser.windows.remove(winId);
        } catch {}
      } else {
        try {
          await browser.tabs.remove(tabId);
        } catch {}
      }
      delete state.metadata.winId;
      delete state.metadata.tabId;
    });
  });

  // Run external action
  window.background.addListener('state.set', ({tabId, newState}) => {
    switch (newState.name) {
      case 'external':
        if (!newState.metadata.status)
          runExternalActionWhenReady(tabId, newState);
        break;
    }
  });
  window.background.addListener('state.remove', async ({tabId, oldState}) => {
    // Cleanup potential external window/tab
    if (oldState.metadata) {
      if (oldState.metadata.winId) {
        try {
          return await browser.windows.remove(oldState.metadata.winId);
        } catch {}
      } else if (oldState.metadata.tabId) {
        try {
          return await browser.tabs.remove(oldState.metadata.tabId);
        } catch {}
      }
    }
  });
})();
