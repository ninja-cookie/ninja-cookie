/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const State = require("../state.js");

(function () {
  window.tabs.addListener('action.focus', async (data, sender) => {
    const state = State.get(sender.tab.id);
    if (!state || state.name !== "external") {
      throw {
        reason: browser.i18n.getMessage('focusNotExternal'),
      };
    }

    // Retrieve current tab if any
    const show = state.metadata.data.options.show;
    let focused;
    if (!show) {
      focused = await browser.tabs.getCurrent();
    }

    // Firefox for Android cannot handle windows
    if (browser.windows && browser.windows.update) {
      await browser.windows.update(sender.tab.windowId, {focused: true});
    }

    // Focus tab
    await browser.tabs.update(sender.tab.id, {active: true});

    // Give it some time in the light
    await new Promise(r => setTimeout(r, 100));

    if (!show) {
      // Firefox for Android cannot handle windows
      if (browser.windows && browser.windows.update) {
        await browser.windows.update(sender.tab.windowId, {focused: false});
      }
    }

    // Focus previous tab if any
    if (focused) {
      // Firefox for Android cannot handle windows
      if (browser.windows && browser.windows.update) {
        await browser.windows.update(focused.windowId, {focused: true});
      }
      await browser.tabs.update(focused.id, {active: true});
    }
  });
})();
