/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const License = require("../license.js");
const Rules = require("../rules.js");
const Storage = require("../storage.js");
const psl = require('psl');

(async function() {
  // Send rules and options to tab when in pending state
  window.background.addListener('state.set', async ({tabId, newState}) => {
    switch (newState.name) {
      case 'pending':
        // Retrieve options
        const options = await License.getOptions();

        // Wait for rules to be loaded
        const rulesData = await Rules.getData();

        // Get rules data and append metadata
        const ruleLists = rulesData.rules;

        // Generate rule from cosmetics filters
        if (Object.keys(rulesData.blockerEngines).length) {
          const hostname = (new URL(newState.metadata.url)).hostname;
          const domain = psl.parse(hostname).domain;
          Object.entries(rulesData.blockerEngines).forEach(
            ([url, engine]) => {
              ruleLists[url] = Object.fromEntries([[
                "metadata", {
                  ...engine.metadata,
                  description: "Automatically generated from Ad Block lists",
                }
              ],[
                "ad-blocker", {
                  "match": [
                    {
                      "type": "adblock",
                      // FIXME: Parsing generated CSS is not ideal...
                      "selector": [...new Set(
                        engine.getCosmeticsFilters({
                          hostname: hostname,
                          domain: domain,

                          getBaseRules: true,
                          getInjectionRules: false,
                          getRulesFromHostname: true,
                          getRulesFromDOM: false,
                        }).styles.replaceAll(
                          ' { display: none !important; }', ''
                        ).replaceAll(
                          ',', ''
                        ).split('\n').filter(l => !!l)
                      )].join(','),
                      "listId": url,
                    },
                  ],
                  "action": [
                    { "type": "hide" },
                    { "type": "warning", "messageId": "autoHideBanner" },
                  ],
                }
              ]]);
            }
          );
        }

        // Send rules and options
        const data = {
          ruleLists,
          options,
        };
        await window.tabs.sendEvent(tabId, {
          type: 'rules.setup',
          data,
        });
        break;
    }
  });

  // Check for rules options update
  window.background.addListener('storage.set', async data => {
    const updatedOptionsKeys = Object.keys(data.options || {});
    const needsUpdate = [
      'defaultLists',
      'customLists',
      'defaultBlockerLists',
      'customBlockerLists',
    ].reduce(
      (acc, key) => acc || updatedOptionsKeys.includes(key),
      false
    );
    if (!needsUpdate)
      return;

    await Rules.getData({force: true});
  });

  // Preload rules data
  await Rules.getData();
})();
