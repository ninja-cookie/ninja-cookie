/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
window.Subscriptions = [];

(function() {
  window.tabs.addListener('subscribe.add', async data => {
    // Queue subscriptions requests
    window.Subscriptions.push(data);

    // Focus/Open options page
    await browser.runtime.openOptionsPage();

    // Send event to options page (if any)
    await window.options.sendEvent({
      type: 'subscribe.add',
      detail: data,
    });
  });
})();
