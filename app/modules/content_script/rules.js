/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Actions = require('../actions.js');

require ('./state.js');

(() => {
  async function setupRules({ruleLists, options}) {
    // Register ruleLists for possible recursive execution
    window.ruleLists = ruleLists;

    return Promise.all(Object.entries(ruleLists).map(([listName, ruleList]) => {
      // Remove metadata if any
      const ruleListMetadata = ruleList.metadata;
      delete ruleList.metadata;

      return Promise.all(Object.entries(ruleList).map(async ([name, rule]) => {
        // Compute rule metadata
        const metadata = Object.fromEntries([[name, {
          ...rule.metadata,
          id: name,
          list: ruleListMetadata,
        }]]);

        // Try to match a rule
        let selected;
        let state;
        try {
          selected = await Actions.execute({
            name: listName + ':' + name,
            metadata,
            actions: rule.match,
            options,
          });
        } catch (err) {
          // Not matched => Silently fail
          if (options.log) {
            console.debug('Rule', name, 'not matched', err);
          }
          return;
        }

        // Matched
        await stateUpdate({
          name: 'matched',
          metadata,
        });

        // Matched => Check required
        try {
          if (rule.required)
            await Actions.execute({
              name: listName + ':' + name,
              metadata,
              actions: rule.required,
              selector: selected,
            });
        } catch (err) {
          // Not required => Done
          if (options.log) {
            console.debug('Rule', name, 'not required', err);
          }
          await stateUpdate({
            name: 'done',
            metadata,
          });
          return;
        }

        let time;
        try {
          // Execute rule actions
          const start = performance.now();
          await Actions.execute({
            name: listName + ':' + name,
            metadata,
            actions: rule.action,
            selector: selected,
          });
          time = performance.now() - start;
        } catch (error) {
          // Actions failed => failed
          await stateUpdate({
            name: 'failed',
            metadata: metadata,
            error,
          });
          return;
        }

        // Actions executed => Done
        await stateUpdate({
          name: 'done',
          metadata,
          time,
        });
      }));
    }));
  }

  if (window.self === window.top) {
    window.background.addListener('rules.setup', setupRules);
  }
})();
