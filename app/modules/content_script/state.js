/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require('jquery');

(async () => {
  async function update(data) {
    if (data.name && data.metadata) {
      for (let k in data.metadata) {
        data.metadata[k].state = data.name;
      }
    }

    state = await window.background.sendEvent({
      type: 'state.update',
      data: data,
    });
  }

  window.stateUpdate = update;

  // Send message to background when ready
  if (window.self === window.top) {
    await new Promise((resolve) => {
      document.addEventListener('DOMContentLoaded', resolve, {once: true});
    });
    await window.background.sendEvent({type: 'state.init'});
  }
})();
