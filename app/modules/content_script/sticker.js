/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require('jquery');

// This is top frame specific
if (window.self === window.top) {
  (() => {
    const sticker = $(
      '<div id="__ninja_cookie_sticker">' +
        '<div class="__ninja_cookie_logo"></div>' +
        '<div class="__ninja_cookie_tooltip"></div>' +
        '<div class="__ninja_cookie_bubbles"></div>' +
      '</div>'
    );
    let stickerEnd;

    function update({position = null, state = null, holiday = null}) {
      // Set position
      if (position) {
        sticker.removeClass(
          (i, c) => c.split(' ').filter(c => c.startsWith('position-')).join(' ')
        ).addClass('position-' + position);
      }

      // Set state
      if (state) {
        sticker.removeClass(
          (i, c) => c.split(' ').filter(c => c.startsWith('state-')).join(' ')
        ).addClass('state-' + state);
        sticker.off('click').on('click', e => showStateBubble(state));

        // Update stateBubble
        if (stateBubble) {
          showStateBubble(state);
        }
      }

      // Set holiday
      if (holiday) {
        sticker.removeClass(
          (i, c) => c.split(' ').filter(c => c.startsWith('holiday-')).join(' ')
        ).addClass(
          'holiday-' + holiday.class
        ).find('.__ninja_cookie_tooltip').attr({
          'data-content': holiday.message
        });
      } else {
        sticker.find('.__ninja_cookie_tooltip').removeAttr('data-content');
      }
    }

    async function show({position = null, state = null, holiday = null, duration = 3000}) {
      if (state === 'matched')
        duration = 0;

      // Wait for DOM to be loaded
      await new Promise(r => $(r));

      // Append elements to DOM
      if (typeof stickerEnd !== 'number') {
        // Add sticker style to current page
        require("../../sass/content_script.scss");
        $(document.body).append(
          sticker.hide().on('click', e => showStateBubble('default'))
        );

        stickerEnd = 0;
      }

      // Update sticker
      update({position, state, holiday});

      // Display forever
      if (stickerEnd === Infinity) {
        return;
      }
      // Display sticker for some time
      const start = Date.now();
      const end = start + duration;
      if (start === end) { // Display until next duration is set
        stickerEnd = end;
      } else if (start > stickerEnd) { // The element has been shown. Show again.
        stickerEnd = end;
      } else if (end > stickerEnd) { // The element should be shown more.
        stickerEnd = end;
        sticker.stop(true);
      } else { // The element should be shown less, so we leave it be.
          return;
      }

      sticker.fadeTo(400, 1);
      if (duration && isFinite(duration)) {
        sticker.delay(duration).fadeTo(400, 0, () => {
          stickerEnd = null;
          sticker.remove();
        });
      }
    };

    async function hide() {
      end = 0;
      sticker.stop(true).fadeTo(400, 0, () => {
        stickerEnd = null;
        sticker.remove();
      });
    };

    async function showBubble(content, callback, data = {}) {
      await show({...data, duration: Infinity});

      const bubbles = $("#__ninja_cookie_sticker .__ninja_cookie_bubbles");
      const bubble = $(
        '<div class="__ninja_cookie_bubble">' +
          '<div class="close"></div>' +
          content +
        '</div>'
      );
      bubbles.append(bubble);
      bubble.callback = callback;
      bubble.on('click', e => {
        e.stopPropagation();

        bubble.hide().remove();
        if (!bubbles.children().length) {
          hide();
        }
        if (bubble.callback) {
          bubble.callback();
        }
      });

      return bubble;
    };


    let messageBubble = null;
    async function showMessage(data) {
      messageBubble = await showBubble(
        browser.i18n.getMessage(data.messageId),
        async () => await window.background.sendEvent({
          type: "sticker.hide_message",
          data,
        }),
        data
      );
    };

    function hideMessage() {
      // Hide message bubble without sending message back to background
      if (messageBubble) {
        delete messageBubble.callback;
        messageBubble.click();
        messageBubble = null;
      }
    };

    let stateBubble = null;
    async function showStateBubble(state) {
      // Update previous state
      const messageId = state + 'StatusDesc';
      if (stateBubble) {
        $("#__ninja_cookie_state").text(browser.i18n.getMessage(messageId));
      } else {
        stateBubble = await showBubble(
          '<p id="__ninja_cookie_state">' + browser.i18n.getMessage(messageId) + '</p>',
          () => stateBubble = null
        );
      }
    };

    // Listen to messages from background
    if (window.self === window.top) {
      window.background.addListener('sticker.show', show);
      window.background.addListener('sticker.update', update);
      window.background.addListener('sticker.show_message', showMessage);
      window.background.addListener('sticker.hide_message', hideMessage);
    }
  })();
}
