/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Config = require("../config.autoload");

(async () => {
  // Subscribe to license key when at given URL
  if (window.location.href.match(Config.subscribeUrl)) {
    const params = Object.fromEntries([...(new URL(window.location)).searchParams]);
    if (params['license']) {
      await window.background.sendEvent({type: 'subscribe.add', data: {
        type: 'license',
        license: params['license'],
      }});
      // TODO : redirect to subscription successful page ?
    }
  }

  // Subscribe to Ninja Cookie/Ad Block list
  // From https://github.com/adblockplus/adblockpluschrome/blob/5b9a51852b9a89d5dfe063afec0e8d7330fcc4e6/subscriptionLink.postload.js
  document.addEventListener("click", async event => {
    // Ignore right-clicks
    if (event.button == 2)
      return;

    // Ignore simulated clicks.
    if (event.isTrusted == false)
      return;

    // Search the link associated with the click
    let link = event.target;
    while (!(link instanceof HTMLAnchorElement))
    {
      link = link.parentNode;

      if (!link)
        return;
    }

    let type = null;
    let queryString = null;
    if (link.protocol == "http:" || link.protocol == "https:")
    {
      type = Config.subscribeDomains[link.host];
      if (type && link.pathname == "/")
        queryString = link.search.substr(1);
    }
    else
    {
      // Firefox 51 doesn't seem to populate the "search" property for
      // links with non-standard URL schemes so we need to extract the query
      // string manually.
      const protocols = Object.keys(Config.subscribeProtocols);
      let regex = new RegExp('^(' + protocols.join('|') + '):\\/*subscribe\\/*\\?(.*)', 'i');
      let match = regex.exec(link.href);
      if (match) {
        type = Config.subscribeProtocols[match[1]];
        queryString = match[2];
      }
    }

    if (!queryString)
      return;

    // This is our link - make sure the browser doesn't handle it
    event.preventDefault();
    event.stopPropagation();

    // Decode URL parameters
    let title = null;
    let url = null;
    for (let param of queryString.split("&"))
    {
      let parts = param.split("=", 2);
      if (parts.length != 2 || !/\S/.test(parts[1]))
        continue;
      switch (parts[0])
      {
        case "title":
          title = decodeURIComponent(parts[1]);
          break;
        case "location":
          url = decodeURIComponent(parts[1]);
          break;
      }
    }
    if (!url)
      return;

    // Default title to the URL
    if (!title)
      title = url;

    // Trim spaces in title and URL
    title = title.trim();
    url = url.trim();
    if (!/^(https?|ftp):/.test(url))
      return;

    await window.background.sendEvent({type: 'subscribe.add', data: {
      type, title, url
    }});
  }, true);
})();