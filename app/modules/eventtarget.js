/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const EventTarget = function() {
  this.listeners = {};
};

EventTarget.prototype.listeners = null;
EventTarget.prototype.addListener = function(type, callback) {
  if (!(type in this.listeners)) {
    this.listeners[type] = [];
  }
  this.listeners[type].push(callback);
};

EventTarget.prototype.addListeners = function(types, callback) {
  for (var i = 0; i < types.length; i++) {
    this.addListener(types[i], callback);
  }
};

EventTarget.prototype.hasListener = function(type, callback) {
  if (!(type in this.listeners)) {
    return false;
  }
  return this.listeners[type].find(listener => listener === callback);
};

EventTarget.prototype.removeListener = function(type, callback) {
  if (!(type in this.listeners)) {
    return;
  }
  const stack = this.listeners[type];
  for (let i = 0, l = stack.length; i < l; i++) {
    if (stack[i] === callback){
      stack.splice(i, 1);
      return;
    }
  }
};

EventTarget.prototype.removeListeners = function(types, callback) {
  for (var i = 0; i < types.length; i++) {
    this.removeListener(types[i], callback);
  }
};

EventTarget.prototype.sendMessage = async function({type, data}, args) {
  if (!(type in this.listeners || '*' in this.listeners)) {
    return;
  }

  const promises = [
    ...(this.listeners[type] || []),
    ...(this.listeners['*'] || [])
  ].map(
    listener => listener.call(null, data, args, type)
  ).filter(
    result => result !== undefined
  );
  if (promises.length === 0) {
    return;
  }

  // Resolve all promises and return the first result
  return (await Promise.all(promises)).find(
    result => result !== undefined
  );
};

module.exports = EventTarget;
