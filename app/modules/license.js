/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Config = require("./config.autoload");
const Storage = require("./storage.js");

// Register License at window level for options/popup page to access it
window.License = {
  _baseUrl: Config.licenseBaseUrl,

  _updateFrequency: Config.licenseUpdateFrequency,
  _infoList: {},

  _getInfo: async function(licenseKey) {
    // Force delay so that data will be truly asynchronous
    // and will not start an infinite loop or run multiple times
    await new Promise(r => setTimeout(r, 1));

    let data = {};
    if (licenseKey) {
      try {
        data = await $.ajax({
          url: License._baseUrl,
          type: 'POST',
          dataType: "json",
          data: JSON.stringify({license: licenseKey}),
        });
      } catch (e) {
        data = e.responseJSON || {};
      }
    }

    return {
      ...data,
      lastUpdate: Date.now(),
      licenseKey,
    };
  },

  getInfo: async function ({
    licenseKey = null,
    force = false,
  } = {}) {
    const defaultLicenseKey = (await Storage.getOptions()).licenseKey;
    if (typeof licenseKey !== "string") {
      licenseKey = defaultLicenseKey;
    }

    if (force) {
      delete License._infoList[licenseKey];
    }

    // Check if old info are available and up-to-date
    const oldInfo = License._infoList[licenseKey];
    if (oldInfo) {
      const nextUpdate = oldInfo.lastUpdate + License._updateFrequency;
      if (nextUpdate > Date.now()) {
        return oldInfo;
      }
    }

    // Save data for next time
    License._infoList[licenseKey] = await License._getInfo(
      licenseKey,
      defaultLicenseKey
    );

    // Dispatch event
    await window.background.sendMessage({
      type: 'license.get',
      data: {
        defaultLicenseKey,
        info : License._infoList[licenseKey],
      },
    });

    // Return new data once retrieved
    return License._infoList[licenseKey];
  },

  getOptions: async function({
    licenseKey = null,
    force = false,
  } = {}) {
    return {
      ...(await Storage.getOptions({force: force})),
      ...(await License.getPremiumOptions({licenseKey, force})),
    };
  },

  getPremiumOptions: async function({
    licenseKey = null,
    force = false,
  } = {}) {
    const info = await License.getInfo({licenseKey, force});

    if (info.status === "active") {
      return {
        ...await Storage.getPremiumOptions({force}),
        licenseKey: info.licenseKey,
      };
    } else {
      return {
        ...Storage._default.premiumOptions,
        licenseKey: info.licenseKey,
      };
    }
  },

  setOptions: async function({
    licenseKey = null,
    force = false,
    data,
  }) {
    return {
      ...(await Storage.setOptions({force, data})),
      ...(await License.setPremiumOptions({licenseKey, force, data})),
    };
  },

  setPremiumOptions: async function({
    licenseKey = null,
    force = false,
    data,
  }) {
    if (!licenseKey && data.licenseKey)
      licenseKey = data.licenseKey;

    const info = await License.getInfo({licenseKey, force});
    if (info.status === "active") {
      return {
        ...await Storage.setPremiumOptions({force, data}),
        licenseKey: info.licenseKey,
      };
    } else {
      return {
        ...Storage._default.premiumOptions,
        licenseKey: info.licenseKey,
      };
    }
  },

  resetOptions: async function({
    licenseKey = null,
    force = false,
    keys = [],
  } = {}) {
    return {
      ...(await Storage.resetOptions({keys})),
      ...(await License.resetPremiumOptions({licenseKey, force, keys})),
    };
  },

  resetPremiumOptions: async function({
    licenseKey = null,
    force = false,
    keys = [],
  } = {}) {
    const info = await License.getInfo({licenseKey, force});
    if (info.status === "active") {
      return {
        ...await Storage.resetPremiumOptions({keys}),
        licenseKey: info.licenseKey,
      };
    } else {
      return {
        ...Storage._default.premiumOptions,
        licenseKey: info.licenseKey,
      };
    }

  },
};

module.exports = License;
