/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
import "bootstrap/js/dist/util";
import "bootstrap/js/dist/alert";
import "bootstrap/js/dist/popover";
import "bootstrap/js/dist/tab";

const $ = require("jquery");

(function() {
  // Show an alert message
  window.alertMessage = function(msg, type = 'success', timeout = 5000) {
    const alert = $(
      '<div class="alert alert-' + type + ' alert-dismissible fade show" role="alert">' +
        msg +
        '<button type="button" class="close py-2" data-dismiss="alert" aria-label="Close">' +
          '<span aria-hidden="true">&times;</span>' +
        '</button>' +
      '</div>'
    ).alert();
    $("#alert-container").append(alert);

    alert.click(e => {
      clearTimeout(alert.timeout);
      delete alert.timeout;
    });
    $('body').click(e => {
      if ($(e.target).hasClass('alert')) {
        return;
      }

      clearTimeout(alert.timeout);
      alert.timeout = setTimeout(() => alert.alert('close'), timeout);
    });
    alert.timeout = setTimeout(() => alert.alert('close'), timeout);
  }

  // Activate tabs
  $('.nav[role="tablist"] a').on('click', function (e) {
    e.preventDefault();
    $(this).tab('show');
    document.location.hash = $(this).attr('href');
  });
  $('.nav[role="tablist"] a[href="' + document.location.hash + '"]').click();

  // Activate popover
  $('.desc').popover( // Display current popover
  ).click(e => {
    $('.desc').not(e.target).popover('hide'); // Hide other popovers
    e.preventDefault(); return false; // Stop click event propagation
  });
  $('body').click(e => {
    if ($(e.target).parents('.popover').length) {
      return;
    }
    $('.desc').popover('hide'); // Hide popover when clicking somewhere else
  });
})();
