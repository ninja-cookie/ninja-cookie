/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Config = require("../config.autoload");

// Load bootstrap alert function
require('./bootstrap.js');

// Load fields getter/setters
require('./disabled.js');
require('./license.js');
require('./rules.js');
require('./storage.js');

(async () => {
  window.Fields = window.Fields || {};

  // Get information from background
  const premiumFields = await window.background.sendMessage({
    type: 'storage.get',
    data: {
      what: 'premiumFields',
    },
  });
  const holiday = await window.background.sendMessage({
    type: 'holiday.get',
  });

  // Setup logo
  if (holiday) {
    $("#header .sticker").addClass(
      'holiday-' + holiday.class
    ).find('.tooltip').attr({
      'data-content': holiday.message
    });
  }

  // Helper functions
  async function valuesSet({init = false, premiumOnly = false}) {
    // Retrieve options
    let data = {};
    if (!init) {
      data = await valuesGet();
    }

    const options = await window.background.sendMessage({
      type: 'license.get',
      data: {
        ...data,
        what: 'options',
      },
    });

    // Set values
    await Promise.all(Object.keys(window.Fields).map(async key => {
      const premium = premiumFields.includes(key);
      if (premiumOnly && !premium)
        return;

      // Set field value and premium status
      const elem = await window.Fields[key].set(options[key]);
      elem.toggleClass('premium', premium);
    }));
    document.dispatchEvent(new CustomEvent('values.set', {
      detail: options,
    }));
  }
  async function valuesGet() {
    return Object.fromEntries(Object.keys(window.Fields).map(
      key => [key, window.Fields[key].get()]
    ));
  }
  async function valuesSave() {
    await window.background.sendMessage({
      type: 'license.set',
      data: {
        what: 'options',
        data: await valuesGet(),
      },
    });
  }
  async function valuesReset() {
    await window.background.sendMessage({
      type: 'license.reset',
      data: {
        what: 'options',
        keys: Object.keys(window.Fields).filter(
          k => window.Fields[k].reset
        ),
      },
    });
  }

  // Setup button callbacks
  $("#save").click(async e => {
    e.preventDefault();
    await valuesSave();
    alertMessage(browser.i18n.getMessage('settingsSaved'));
  });
  $("#cancel").click(async e => {
    e.preventDefault();
    await valuesSet({init: true});
    alertMessage(browser.i18n.getMessage('settingsCanceled'), "info");
  });
  $("#reset").click(async e => {
    e.preventDefault();
    if (confirm(browser.i18n.getMessage('settingsResetConfirm'))) {
      await valuesReset();
      await valuesSet({init: true});
      alertMessage(browser.i18n.getMessage('settingsReseted'));
    } else {
      alertMessage(browser.i18n.getMessage('settingsNotReseted'), "info");
    }
  });

  // Setup subscription button
  $("#subscription a").prop('href', Config.subscriptionUrl);

  // Listen to values update requests
  document.addEventListener('values.update', async event => {
    const {premiumOnly} = event.detail;
    // Update values
    await valuesSet({init: false, premiumOnly});
  });

  // Init values
  await valuesSet({init: true});
})();
