/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const isValidDomain = require('is-valid-domain')

// Load bootstrap alert function
require('./bootstrap.js');

(async () => {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Escape string for HTML context
  function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
  }

  // Add a domain name to whitelist container
  async function domainAdd({
    container,
    domain,
    alreadyExists = null,
  }) {
    if (alreadyExists && $("[data-domain='" + domain + "']").length) {
      alertMessage(
        browser.i18n.getMessage(alreadyExists, escapeHtml(domain)),
        "danger"
      );
      return false;
    }

    domain = domain.replace(/^www\./, '').toLowerCase();
    if (!isValidDomain(domain, {subdomain: true})) {
      alertMessage(
        // @desc The error message displayed when trying to add an invalid domain name
        // @param domain=foobar
        browser.i18n.getMessage('settingsWhitelistBadDomain',
          escapeHtml(domain)
        ),
        "danger"
      );
      return false;
    }

    // Remove same domain
    $("[data-domain='" + domain + "']").remove();

    const div = $(
      '<label class="row list" data-domain="' + domain + '">' +
        '<div class="col my-auto">' +
          '<code class="name">' + domain + '</code>' +
        '</div>' +
        '<div class="col col-auto">' +
          '<div class="d-inline-block align-middle ml-2">' +
            '<button type="button" class="close" aria-label="Remove">' +
              '<span aria-hidden="true">&times;</span>' +
            '</button>' +
          '</div>' +
        '</div>' +
      '</label>'
    );
    div.find('button').on('click', i => {
      $("[data-domain='" + domain + "']").remove();
    });
    const next = [...$(container + ' [data-domain]')].find(
      elem => domain < $(elem).attr('data-domain')
    );
    if (next) {
      $(next).before(div);
    } else {
      $(container).append(div);
    }

    return true;
  }

  // Add a list of domain to whitelist container
  async function whitelistAdd({
    container,
    domains,
    append = false,
    alreadyExists = null,
  }) {
    if (!append)
      $(container).empty();

    return (await Promise.all(domains.map(
      async domain => domainAdd({
        container,
        domain,
        alreadyExists,
      }) ? null : domain
    ))).filter(domain => !!domain);
  }

  // Init whitelist
  function whitelistSetup({form, container, alreadyExists}) {
    // Trigger button when enter key is pressed
    $(form + ' input').keyup(async e => {
      if (e.keyCode === 13) { // Enter
        $(form + ' button').click();
      }
    });
    // Setup button callback
    $(form + ' button').click(async e => {
      e.preventDefault();
      const domains = $(form + ' input').val();
      const domainAdded = await whitelistAdd({
        container,
        domains: domains.split(','),
        append: true,
        alreadyExists: alreadyExists
      });
      if (domainAdded) {
        $(form + ' input').val('');
      }
    });

    return {
      get: () => [
        ...$(container + " .list").map(
          (i, l) => $(l).data('domain')
        )
      ],
      set: async domains => {
        await whitelistAdd({
          container,
          domains,
        });
        return $([form, container].join(','));
      },
      reset: true,
    };
  }

  // Add fields setters/getters
  window.Fields = {
    ...(window.Fields || {}),
    whitelist: whitelistSetup({
      form: "#whitelist-add",
      container: "#whitelist-container",
      // @desc The error message displayed when trying to add domain already in the list
      // @param domain=marmiton.org
      alreadyExists: getMessage('settingsWhitelistAlreadyExists'),
    })
  };
})();
