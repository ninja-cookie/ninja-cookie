/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(() => {
  // TODO:
  // A nice solution would be to use html attributes (eg: 'data-i18n-content')
  // to associate HTML element to a i18n message ID.
  // The problem with this solution is poedit message ID detection.

  // Page
  $("title").text(browser.i18n.getMessage('settingsTitle'));

  // Header
  $("#header img").prop("alt", browser.i18n.getMessage('settingsLogo'));
  $("#header h1").text(browser.i18n.getMessage('settingsHeader'));

  // Tabs
  $("#general").text(browser.i18n.getMessage('settingsGeneral'));
  $("#advanced").text(browser.i18n.getMessage('settingsAdvanced'));
  $("#premium").text(browser.i18n.getMessage('settingsPremium'));

  // Display settings
  $("#display .header").text(browser.i18n.getMessage('settingsDisplay'));
  $("#sticker .header").text(browser.i18n.getMessage('settingsSticker'));
  $("#sticker .desc").attr('data-content', browser.i18n.getMessage('settingsStickerDesc'));
  $("#sticker-position .header").text(browser.i18n.getMessage('settingsStickerPosition'));
  $("#sticker-position .desc").attr('data-content', browser.i18n.getMessage('settingsStickerPositionDesc'));
  $("#sticker-position option[value='top-left']").text(browser.i18n.getMessage('settingsStickerPositionTopLeft'));
  $("#sticker-position option[value='top-right']").text(browser.i18n.getMessage('settingsStickerPositionTopRight'));
  $("#sticker-position option[value='bottom-left']").text(browser.i18n.getMessage('settingsStickerPositionBottomLeft'));
  $("#sticker-position option[value='bottom-right']").text(browser.i18n.getMessage('settingsStickerPositionBottomRight'));
  $("#live .header").text(browser.i18n.getMessage('settingsLive'));
  $("#live .desc").attr('data-content', browser.i18n.getMessage('settingsLiveDesc'));

  // Whitelist settings
  $("#whitelist .header").text(browser.i18n.getMessage('settingsWhitelist'));
  $("#whitelist .desc").attr('data-content', browser.i18n.getMessage('settingsWhitelistDesc'));
  $("#whitelist-add .header").text(browser.i18n.getMessage('settingsWhitelistAdd'));
  $("#whitelist-add .desc").attr('data-content', browser.i18n.getMessage('settingsWhitelistAddDesc'));
  $("#whitelist-add input").prop("placeholder", browser.i18n.getMessage('settingsWhitelistAddPlaceholder'));
  $("#whitelist-add input").prop("aria-describedby", browser.i18n.getMessage('settingsWhitelistAddPlaceholder'));
  $("#whitelist-add button").text(browser.i18n.getMessage('settingsWhitelistAddButton'));
  $("#whitelist-container .loading").text(browser.i18n.getMessage('settingsLoading'));

  // NC Lists settings
  $("#nc-lists .header").text(browser.i18n.getMessage('settingsNCLists'));
  $("#nc-lists .desc").attr('data-content', browser.i18n.getMessage('settingsNCListsDesc'));
  $("#nc-lists-default .header").text(browser.i18n.getMessage('settingsNCListsDefault'));
  $("#nc-lists-default .desc").attr('data-content', browser.i18n.getMessage('settingsNCListsDefaultDesc'));
  $("#nc-lists-default option[value='stable']").text(browser.i18n.getMessage('settingsNCListsDefaultStable'));
  $("#nc-lists-default option[value='rc']").text(browser.i18n.getMessage('settingsNCListsDefaultRC'));
  $("#nc-lists-default option[value='beta']").text(browser.i18n.getMessage('settingsNCListsDefaultBeta'));
  $("#nc-lists-default option[value='none']").text(browser.i18n.getMessage('settingsNCListsDefaultNone'));
  $("#nc-lists-custom .header").text(browser.i18n.getMessage('settingsNCListsCustom'));
  $("#nc-lists-custom .desc").attr('data-content', browser.i18n.getMessage('settingsNCListsCustomDesc'));
  $("#nc-lists-custom input").prop("placeholder", browser.i18n.getMessage('settingsNCListsCustomPlaceholder'));
  $("#nc-lists-custom input").prop("aria-describedby", browser.i18n.getMessage('settingsNCListsCustomPlaceholder'));
  $("#nc-lists-custom button").text(browser.i18n.getMessage('settingsNCListsCustomButton'));
  $("#nc-lists-container .loading").text(browser.i18n.getMessage('settingsLoading'));

  // AB Lists settings
  $("#ab-lists .header").text(browser.i18n.getMessage('settingsABLists'));
  $("#ab-lists .desc").attr('data-content', browser.i18n.getMessage('settingsABListsDesc'));
  $("#ab-lists-enabled .header").text(browser.i18n.getMessage('settingsABListsEnabled'));
  $("#ab-lists-enabled .desc").attr('data-content', browser.i18n.getMessage('settingsABListsEnabledDesc'));
  $("#ab-lists-default .header").text(browser.i18n.getMessage('settingsABListsDefault'));
  $("#ab-lists-default .desc").attr('data-content', browser.i18n.getMessage('settingsABListsDefaultDesc'));
  $("#ab-lists-default option[value='stable']").text(browser.i18n.getMessage('settingsABListsDefaultStable'));
  $("#ab-lists-default option[value='none']").text(browser.i18n.getMessage('settingsABListsDefaultNone'));
  $("#ab-lists-custom .header").text(browser.i18n.getMessage('settingsABListsCustom'));
  $("#ab-lists-custom .desc").attr('data-content', browser.i18n.getMessage('settingsABListsCustomDesc'));
  $("#ab-lists-custom input").prop("placeholder", browser.i18n.getMessage('settingsABListsCustomPlaceholder'));
  $("#ab-lists-custom input").prop("aria-describedby", browser.i18n.getMessage('settingsABListsCustomPlaceholder'));
  $("#ab-lists-custom button").text(browser.i18n.getMessage('settingsABListsCustomButton'));
  $("#ab-lists-container .loading").text(browser.i18n.getMessage('settingsLoading'));

  // Debug settings
  $("#debug .header").text(browser.i18n.getMessage('settingsDebug'));
  $("#debug .desc").attr('data-content', browser.i18n.getMessage('settingsDebugDesc'));
  $("#trust-all .header").text(browser.i18n.getMessage('settingsTrustAll'));
  $("#trust-all .desc").attr('data-content', browser.i18n.getMessage('settingsTrustAllDesc'));
  $("#log .header").text(browser.i18n.getMessage('settingsLog'));
  $("#log .desc").attr('data-content', browser.i18n.getMessage('settingsLogDesc'));

  // License settings
  $("#license .header").text(browser.i18n.getMessage('settingsLicense'));
  $("#license .desc").attr('data-content', browser.i18n.getMessage('settingsLicenseDesc'));
  $("#license-key .header").text(browser.i18n.getMessage('settingsLicenseKey'));
  $("#license-key .desc").attr('data-content', browser.i18n.getMessage('settingsLicenseKeyDesc'));
  $("#license-key input").prop("placeholder", browser.i18n.getMessage('settingsLicenseKeyPlaceholder'));
  $("#license-key input").prop("aria-describedby", browser.i18n.getMessage('settingsLicenseKeyPlaceholder'));
  $("#license-key button").text(browser.i18n.getMessage('settingsLicenseKeyButton'));
  $("#license-info .loading").text(browser.i18n.getMessage('settingsLoading'));

  // License settings
  $("#subscription a").text(browser.i18n.getMessage('settingsSubscription'));
  $("#subscription small").html(
    // @desc The message shown under the license info
    // @param amount=0€
    browser.i18n.getMessage('settingsSubscriptionSubtitle', [
      Number(0).toLocaleString(undefined, {
        style: "currency",
        currency: 'EUR',
      })
    ])
  );

  // Buttons
  $("#save").text(browser.i18n.getMessage('settingsSave'));
  $("#cancel").text(browser.i18n.getMessage('settingsCancel'));
  $("#reset").text(browser.i18n.getMessage('settingsReset'));
})();
