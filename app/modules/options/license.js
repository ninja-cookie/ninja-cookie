/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
window.$ = require("jquery");

// Load bootstrap alert function
require('./bootstrap.js');

(async () => {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Escape string for HTML context
  function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
  }

  // Set input disable state
  async function setPremiumFields({licenseKey = null}) {
    const info = await window.background.sendMessage({
      type: 'license.get',
      data: {
        what: 'info',
        licenseKey,
      },
    });
    const enabled = info.status === 'active';

    $('.premium').toggleClass('disabled', !enabled);
    $('.premium').attr({
      'data-toggle': 'popover',
      'data-html': 'true',
      'data-trigger': 'hover',
      'data-placement': 'left',
      'data-content': browser.i18n.getMessage('settingsPremiumTooltip'),
    })
    $('.premium').popover(enabled ? 'disable': 'enable');
    $('.premium :input, .premium label').prop('disabled', !enabled);
  }

  // Set license container
  async function licenseSet({container, licenseKey = null, force = false}) {
    // Special case => cleanup license info
    if (licenseKey === '') {
      $(container).empty();
      document.dispatchEvent(new CustomEvent('values.update', {
        detail: {
          premiumOnly: true,
        },
      }));
      return false;
    }

    // Check license format
    if (licenseKey && !licenseKey.match(/^[0-9A-F]{7}-[0-9A-F]{7}-[0-9A-F]{7}$/)) {
      alertMessage(
        // @desc The error message shown when a license key with a bad format is provided
        // @param license=0123456-789ABCD-EF01234
        browser.i18n.getMessage('licenseBadFormat', [
          escapeHtml(licenseKey)
        ]),
        "danger"
      );
      return false;
    }

    // Remove previous license info
    $(container).empty();

    // Get license information
    const data = await window.background.sendMessage({
      type: 'license.get',
      data: {
        what: 'info',
        licenseKey,
        force,
      },
    });

    // Setup license field if available
    if (data.licenseKey) {
      // Format information
      const status = {
        active: browser.i18n.getMessage('licenseStatusActive'),
        inactive: browser.i18n.getMessage('licenseStatusInactive'),
      }[data.status];
      let error_message;
      if (data.error_code) {
        error_message = {
          bad_method: browser.i18n.getMessage('licenseErrorBadMethod'),
          bad_parameter: browser.i18n.getMessage('licenseErrorBadParameter'),
          internal_error: browser.i18n.getMessage('licenseErrorInternal'),
          license_not_found: browser.i18n.getMessage('licenseErrorNotFound'),
          missing_parameter: browser.i18n.getMessage('licenseErrorMissingParameter'),
        }[data.error_code] || browser.i18n.getMessage('licenseErrorUnknown');
      }
      let start;
      if (typeof data.start_date === "number") {
        const startDate = new Date(data.start_date * 1000);
        // @desc The creation date of license subscription
        // @param date=11/17/2020
        // @param time=11:07:38 AM
        start = browser.i18n.getMessage('licenseStartDate', [
          startDate.toLocaleString(undefined, {
            year: 'numeric', month: 'numeric', day: 'numeric',
          }),
          startDate.toLocaleString(undefined, {
            hour: 'numeric', minute: 'numeric', second: 'numeric',
          }),
        ]);
      }
      let end;
      if (typeof data.cancel_at === "number") {
        const endDate = new Date(data.cancel_at * 1000);
        let msg;
        if (endDate > Date.now()) {
          // @desc The expiry date of license subscription (in the future)
          // @param date=11/17/2020
          // @param time=11:07:38 AM
          msg = getMessage('licenseExpiryDateFuture');
        } else {
          // @desc The expiry date of license subscription (in the past)
          // @param date=11/17/2020
          // @param time=11:07:38 AM
          msg = getMessage('licenseExpiryDatePast');
        }
        end = browser.i18n.getMessage(msg, [
          endDate.toLocaleString(undefined, {
            year: 'numeric', month: 'numeric', day: 'numeric',
          }),
          endDate.toLocaleString(undefined, {
            hour: 'numeric', minute: 'numeric', second: 'numeric',
          }),
        ]);
      }
      let fee;
      if (typeof data.plan_interval === "string" && typeof data.plan_amount === "number" && typeof data.plan_currency === "string") {
        let msg;
        switch (data.plan_interval) {
          case "day":
            // @desc The daily fee associated to a license subscription
            // @param amount=$2.00
            msg = getMessage('licenseDailyFee');
            break;
          case "week":
            // @desc The weekly fee associated to a license subscription
            // @param amount=$2.00
            msg = getMessage('licenseWeeklyFee');
            break;
          case "month":
            // @desc The monthly fee associated to a license subscription
            // @param amount=$2.00
            msg = getMessage('licenseMonthlyFee');
            break;
          case "year":
            // @desc The yearly fee associated to a license subscription
            // @param amount=$2.00
            msg = getMessage('licenseYearlyFee');
            break;
        }
        fee = browser.i18n.getMessage(msg, [
          Number(data.plan_amount/100).toLocaleString(undefined, {
            style: "currency",
            currency: data.plan_currency,
          }),
        ]);
      }

      // Setup license element
      const div = $(
        '<label class="row license">' +
          '<div class="col my-auto">' +
            '<code class="key">' + data.licenseKey + '</code>' +
            (
              error_message ?
              '<small class="text-secondary error">' +
                ' - ' + error_message +
              '</small>' : ''
            ) +
            (
              status ?
              '<small class="text-secondary status">' +
                ' - ' + status +
              '</small>' : ''
            ) +
            (
              end ?
              '<small class="text-secondary end">' +
                ' - ' + end + '' +
              '</small>' :
              (
                start ?
                '<small class="text-secondary start">' +
                  ' - ' + start + '' +
                '</small>' : ''
              )
            ) +
            (
              fee ?
              '<small class="text-secondary fee">' +
                ' - ' + fee + '' +
              '</small>' : ''
            ) +
          '</div>' +
          '<div class="col col-auto">' +
            '<div class="d-inline-block align-middle ml-2">' +
              '<button type="button" class="reload" aria-label="Reload">' +
                  '<span aria-hidden="true">&#8635;</span>' +
              '</button>' +
            '</div>' +
            '<div class="d-inline-block align-middle ml-2">' +
              '<button type="button" class="close" aria-label="Remove">' +
                '<span aria-hidden="true">&times;</span>' +
              '</button>' +
            '</div>' +
          '</div>' +
        '</label>'
      );
      div.find('button.reload').on('click', async i => {
        // Update license info
        await licenseSet({
          container,
          licenseKey,
          force: true,
        });
      });
      div.find('button.close').on('click', async i => {
        // Remove license info
        await licenseSet({
          container,
          licenseKey: '',
        });

        // Display information message when removing active license
        if (data.status === "active") {
          alertMessage(
            browser.i18n.getMessage('settingsLicenseKeyRemoved'),
            "info",
            10000
          );
        }
      });
      $(container).append(div);

      if (data.status !== "active") {
        alertMessage(
          // @desc The message shown after a license key has been added but is not active
          // @param license=0123456-789ABCD-EF01234
          browser.i18n.getMessage('settingsLicenseKeyError', [
            data.licenseKey,
          ]),
          "warning"
        );
      }

      document.dispatchEvent(new CustomEvent('values.update', {
        detail: {
          premiumOnly: true,
        },
      }));
    }

    // Return false if error
    return data.status === "active";
  }

  function licenseSetup({form, container,}) {
    // Trigger button when enter key is pressed
    $(form + ' input').keyup(async e => {
      if (e.keyCode === 13) { // Enter
        $(form + ' button').click();
      }
    });
    // Setup button callback
    $(form + ' button').click(async e => {
      e.preventDefault();
      const licenseKey = $(form + ' input').val();
      const licenseAdded = await licenseSet({
        container,
        licenseKey,
      });
      if (licenseAdded) {
        $(form + ' input').val('');
      }
    });

    return {
      get: () => $(container + " .key").text(),
      set: async licenseKey => {
        await licenseSet({
          container,
          licenseKey,
        });
        return $([form, container].join(','));
      },
      reset: false,
    };
  }

  // Add fields setters/getters
  window.Fields = {
    ...(window.Fields || {}),
    licenseKey: licenseSetup({
      form: "#license-key",
      container: "#license-info",
    }),
  };

  // Wait for values to be set before handling premium fields
  document.addEventListener('values.set', async event => {
    const data = event.detail;
    await setPremiumFields(data);
  });
})();
