/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

// Load bootstrap alert function
require('./bootstrap.js');

(async () => {
  // Method used to force i18n string detection
  const getMessage = s => s;

  // Escape string for HTML context
  function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
  }

  // Check whether the argument is a valid URL
  function isUrl(url) {
    try {
      new URL(url);
      return true;
    } catch {
      return false;
    }
  }

  // Convert a list URL to an object containing display data
  async function convertList(url, force) {
    // Check URL
    if (!isUrl(url)) {
      alertMessage(
        // @desc The error message shown when a custom Ninja Cookie list is not a valid URL
        // @param url=foobar
        browser.i18n.getMessage('settingsNCListsCustomBadURL', [
          escapeHtml(url)
        ]),
        "danger"
      );
      return false;
    }

    // Try to retrieve list relevant data
    const list = await window.background.sendMessage({
      type: 'rules.get',
      data: {
        what: 'listInfo',
        url,
        force,
      },
    });
    if (!list) {
      alertMessage(
        // @desc The error message shown when a custom Ninja Cookie list loading fails
        // @param url=https://ninja-cookie.gitlab.io/rules/cmp.json
        browser.i18n.getMessage('settingsNCListsCustomLoadError', [
          escapeHtml(url)
        ]),
        "danger"
      );
      return false;
    }

    return list;
  }

  // Convert an engine URL to an object containing display data
  async function convertEngine(url, force) {
    // Check URL
    if (!isUrl(url)) {
      alertMessage(
        // @desc The error message shown when a custom Ad Blocker list is not a valid URL
        // @param url=foobar
        browser.i18n.getMessage('settingsABListsCustomBadURL', [
          escapeHtml(url)
        ]),
        "danger"
      );
      return false;
    }

    // Try to retrieve engine relevant data
    const engine = await window.background.sendMessage({
      type: 'rules.get',
      data: {
        what: 'blockerListInfo',
        url,
        force,
      },
    });
    if (!engine) {
      alertMessage(
        // @desc The error message shown when a custom Ad Blocker list loading fails
        // @param url=https://www.fanboy.co.nz/fanboy-cookiemonster.txt
        browser.i18n.getMessage('settingsABListsCustomLoadError', [
          escapeHtml(url)
        ]),
        "danger"
      );
      return false;
    }

    return engine;
  }


  // Add a list info to DOM
  async function listAdd({
    container,
    url,
    converter,
    custom = false,
    alreadyExists = null,
    force = false,
  }) {
    if (alreadyExists && $("[data-href='" + url + "']").length) {
      alertMessage(
        browser.i18n.getMessage(alreadyExists, [escapeHtml(url)]),
        "danger"
      );
      return false;
    }

    const list = await converter(url, force);
    if (!list) {
      return false;
    }

    // Remove list with same URL
    $("[data-href='" + url + "']").remove();

    const div = $(
      '<label class="row list ' + ( custom ? ' custom' : 'default' ) + '" data-href="' + url + '">' +
        '<div class="col my-auto">' +
          '<code class="name">' + list.name + '</code>' +
          (
            list.version ?
            '<small class="text-secondary version">' +
              ' - ' + list.version +
            '</small>' : ''
          ) +
          (
            list.count ?
            '<small class="text-secondary count">' +
              ' - ' + list.count + ' rules' +
            '</small>' : ''
          ) +
        '</div>' +
        '<div class="col col-auto">' +
          '<div class="d-inline-block align-middle ml-2">' +
            '<button type="button" class="reload" aria-label="Reload">' +
                '<span aria-hidden="true">&#8635;</span>' +
            '</button>' +
          '</div>' +
          (
            custom ?
            '<div class="d-inline-block align-middle ml-2">' +
              '<button type="button" class="close" aria-label="Remove">' +
                '<span aria-hidden="true">&times;</span>' +
              '</button>' +
            '</div>' : ''
          ) +
        '</div>' +
      '</label>'
    );
    div.find('button.reload').on('click', async i => {
      // Re-add list but force update
      listAdd({
        container,
        url,
        converter,
        custom,
        alreadyExists: null,
        force: true,
      });
    });
    div.find('button.close').on('click', i => {
      $("[data-href='" + url + "']").remove();
    });
    $(container).append(div);

    return true;
  }

  // Setup a list value
  async function listValuesSetup({
    container,
    converter,
    urls,
    custom = false,
    append = false,
    alreadyExists = null
  }) {
    if (!append)
      $(container).empty();

    return (await Promise.all(urls.map(
      async url => listAdd({
        container,
        converter,
        url,
        custom,
        alreadyExists,
      }) ? null : url
    ))).filter(url => !!url);
  }


  // Setup default list container
  function defaultListSetup({form, container, converter, options}) {
    // Setup input
    $(form + ' select').change(async e => {
      e.preventDefault();
      const value = $(form + ' select').val();
      await listValuesSetup({
        container,
        converter,
        urls: options[value],
      });
    });

    // Return getter/setter
    return {
      get: () => $(form + ' select').val(),
      set: async value => {
        $(form + ' select').val(value);
        await listValuesSetup({
          container,
          converter,
          urls: options[value],
        });
        return $([form, container].join(','));
      },
      reset: true,
    };
  }

  // Setup custom list container
  function customListSetup({form, container, converter, alreadyExists}) {
    // Trigger button when enter key is pressed
    $(form + ' input').keyup(async e => {
      if (e.keyCode === 13) { // Enter
        $(form + ' button').click();
      }
    });
    // Setup button callback
    $(form + ' button').click(async e => {
      e.preventDefault();
      const urls = $(form + ' input').val();
      if (!urls)
        return;

      $(form + ' input').val((await listValuesSetup({
        container,
        converter,
        urls: urls.split(','),
        custom: true,
        append: true,
        alreadyExists,
      })).join(','));
    });

    // Return getter/setter
    return {
      get: () => [
        ...$(container + " .list").map(
          (i, l) => $(l).data('href')
        )
      ],
      set: async urls => {
        await listValuesSetup({
          container,
          converter,
          urls,
          custom: true,
        });
        return $([form, container].join(','));
      },
      reset: true,
    };
  }

  // Get background modules
  const {lists, blockerLists} = await window.background.sendMessage({
    type: 'rules.get',
    data: {
      what: 'defaultLists',
    },
  });

  // Add fields setters/getters
  window.Fields = {
    ...(window.Fields || {}),
    defaultLists: defaultListSetup({
      form: "#nc-lists-default",
      container: "#nc-lists-container",
      converter: convertList,
      options: lists,
    }),
    defaultBlockerLists: defaultListSetup({
      form: "#ab-lists-default",
      container: "#ab-lists-container",
      converter: convertEngine,
      options: blockerLists,
    }),
    customLists: customListSetup({
      form: "#nc-lists-custom",
      container: "#nc-lists-custom-container",
      converter: convertList,
      // @desc The error message shown when a custom Ninja Cookie list is already in the default lists
      // @param url=https://ninja-cookie.gitlab.io/rules/cmp.json
      alreadyExists: getMessage('settingsNCListsCustomAlreadyExists'),
    }),
    customBlockerLists: customListSetup({
      form: "#ab-lists-custom",
      container: "#ab-lists-custom-container",
      converter: convertEngine,
      // @desc The error message shown when a custom Ad Blocker list is already in the default lists
      // @param url=https://www.fanboy.co.nz/fanboy-cookiemonster.txt
      alreadyExists: getMessage('settingsABListsCustomAlreadyExists'),
    }),
  };
})();