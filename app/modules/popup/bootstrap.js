/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
import "bootstrap/js/dist/util";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/popover";

const $ = require("jquery");

(function() {
  // Activate popover
  $('.desc').popover( // Display current popover
  ).click(e => {
    $('.desc').not(e.target).popover('hide'); // Hide other popovers
    e.preventDefault(); return false; // Stop click event propagation
  });
  $('body').click(e => {
    if ($(e.target).parents('.popover').length) {
      return;
    }
    $('.desc').popover('hide'); // Hide popover when clicking somewhere else
  });
})();
