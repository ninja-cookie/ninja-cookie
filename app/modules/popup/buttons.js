/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Config = require("../config.autoload");

(function() {
  const tabId = window.tabId;

  async function openUrl(url) {
    const tabs = await browser.tabs.query({active: true, lastFocusedWindow: true});
    const tab = tabs[0];

    if (browser.windows && browser.windows.create) {
      // Because Firefox does not support 'focus' option during window creation
      // we have to update the window once created to focus it.
      const win = await browser.windows.create({
        url: url,
        width: 580,
        height: 560,
        type: "popup",
      });
      await browser.windows.update(win.id, { focused: true });
    } else { // In Firefox For Android, create a new tab
      await browser.tabs.create({
        url: url,
        active: true,
      });
    }
  }

  // Setup settings button
  $("#settings a").click(async event => {
    event.preventDefault();
    window.background.sendEvent({
      type: 'options.open'
    });
  });

  // Setup report/suggest buttons
  $("#report").click(async e => {
    const state = await window.background.sendMessage({
      type: 'state.get',
      data: tabId,
    });
    const matchedRules = Object.entries(state.metadata || {}).filter(([k, v]) => {
      return typeof v === 'object';
    }).map(([k, v]) => ({
      name: k,
      state: v.state,
      url: v.url || (v.list || {}).url,
      version: v.updated || (v.list || {}).version,
    }));

    // @desc The URL of the report form
    const url = browser.i18n.getMessage('reportUrl') + '?' + $.map({
      'message64': btoa(browser.i18n.getMessage(state.report.messageId, [
        (state.metadata || await browser.tabs.get(tabId)).url,
        browser.runtime.getManifest().version,
        navigator.userAgent,
        JSON.stringify(matchedRules),
      ])),
      'category': state.report.category,
    }, (v, k) => k + '=' + encodeURIComponent(v)).join('&')
    await openUrl(url);
  });
  $("#suggest").click(async e => {
    // @desc The URL of the suggestion form
    const url = browser.i18n.getMessage('suggestUrl') + '?' + $.map({
      // @desc The default message displayed in the suggestion form
      'message64': btoa(browser.i18n.getMessage('suggestMessage')),
    }, (v, k) => k + '=' + encodeURIComponent(v)).join('&')
    await openUrl(url);
  });

  // Setup subscription button
  $("#subscription a").prop('href', Config.subscriptionUrl);
})();
