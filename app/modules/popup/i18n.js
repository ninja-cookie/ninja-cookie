/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(() => {
  // TODO:
  // A nice solution would be to use html attributes (eg: 'data-i18n-content')
  // to associate HTML element to a i18n message ID.
  // The problem with this solution is poedit message ID detection.

  // Page messages
  $("title").text(browser.i18n.getMessage('popupTitle'));

  // Settings, pause and whitelist buttons
  $("#settings button").prop("title", browser.i18n.getMessage('popupSettings'));
  $("#disable button").text(browser.i18n.getMessage('popupDisable'));
  $("#disable .dropdown-item[data-target='duration'][data-duration='30']").text(browser.i18n.getMessage('popupDisableTab30min'));
  $("#disable .dropdown-item[data-target='duration'][data-duration='Infinity']").text(browser.i18n.getMessage('popupDisableTabForever'));
  $("#enable button").text(browser.i18n.getMessage('popupEnable'));

  // Bug/Suggestion buttons
  $("#content .header").text(browser.i18n.getMessage('popupDesc'));
  $("#report").text(browser.i18n.getMessage('popupReportBtn'));
  $("#suggest").text(browser.i18n.getMessage('popupSuggestBtn'));

  // Stats
  $("#stats-banners .header").text(browser.i18n.getMessage('popupStatsBanner'));
  $("#stats-time .header").text(browser.i18n.getMessage('popupStatsTime'));

  // Share buttons
  $("#share .facebook").attr('title', browser.i18n.getMessage('popupShareFacebookTitle'));
  $("#share .twitter").attr('title', browser.i18n.getMessage('popupShareTweeterTitle'));
  $("#share .review").attr('title', browser.i18n.getMessage('popupShareReviewTitle'));

  // Subscription buttons
  $("#subscription a").text(browser.i18n.getMessage('popupSubscription'));
  $("#subscription small").html(
    // @desc The message shown under the subscription button
    // @param amount=0€
    browser.i18n.getMessage('popupSubscriptionSubtitle', [
      Number(0).toLocaleString(undefined, {
        style: "currency",
        currency: 'EUR',
      })
    ])
  );

  // Social buttons
  $("#social .header").text(browser.i18n.getMessage('popupSocialDesc'));
  $("#social .website").attr('title', browser.i18n.getMessage('popupSocialWebsite'));
  $("#social .facebook").attr('title', browser.i18n.getMessage('popupSocialFacebookTitle'));
  $("#social .twitter").attr('title', browser.i18n.getMessage('popupSocialTwitterTitle'));
})();
