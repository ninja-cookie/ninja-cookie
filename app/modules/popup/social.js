/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const Config = require('../config.autoload');
const { detect } = require('detect-browser');

(async () => {
  // Setup Facebook button
  $("#share .facebook").attr(
    'href',
    "https://www.facebook.com/dialog/feed?" + $.map({
      'app_id': Config.facebookAppId,
      'display': 'page',
      'link': Config.website,
      'hashtag': '#' + Config.facebookHashtag,
    }, (v, k) => encodeURIComponent(k) + '=' + encodeURIComponent(v)).join('&')
  );

  // Setup rating button
  const detectedBrowser = detect();
  if (detectedBrowser && detectedBrowser.name in Config.reviewUrls) {
    $("#share .review").toggleClass('d-none', false);
    $("#share .review").attr('href', Config.reviewUrls[detectedBrowser.name]);
  } else {
    $("#share .review").toggleClass('d-none', true);
  }

  // Setup links
  $("#social .website").attr('href', Config.website);
  $("#social .facebook").attr(
    'href',
    "https://www.facebook.com/" + Config.facebookAccount + "/"
  );
  $("#social .twitter").attr(
    'href',
    "https://twitter.com/" + Config.twitterAccount
  );

  // Return localized unit and time from milliseconds
  function formatTime(time) {
    time /= 1000;
    let unit = browser.i18n.getMessage('seconds');
    if (time > 60) {
      time /= 60; unit = browser.i18n.getMessage('minutes');
      if (time > 60) {
        time /= 60; unit = browser.i18n.getMessage('hours');
        if (time > 24) {
          time /= 24; unit = browser.i18n.getMessage('days');
          if (time > 365) {
            time /= 365; unit = browser.i18n.getMessage('years');
          }
        }
      }
    }

    return {time, unit};
  }

  // Update stats
  function update(stats) {
    const actual = formatTime(stats.totalTime);
    const saved = formatTime(stats.totalTime * Config.savingFactor);

    // Update stats
    $("#stats-banners .count").text(stats.totalBanner);
    $("#stats-time .count").text(Number(saved.time).toFixed(1));
    $("#stats-time .unit").text(saved.unit);
    $("#stats-time .desc").attr('data-content',
      // @desc The message explaining stats and saving factor
      // @param factor=5
      // @param time=12
      // @param unit=seconds
      browser.i18n.getMessage('popupStatsDesc', [
        '' + Config.savingFactor,
        '' + Number(actual.time).toFixed(1),
        actual.unit
      ])
    );

    // Update share Tweet content
    $("#share .twitter").attr(
      'href',
      'https://twitter.com/intent/tweet?text=' + encodeURIComponent(
        // @desc The prefilled content of a Tweet
        // @param account=@ninjacookie
        // @param count=12
        // @param time=50
        // @param unit=seconds
        browser.i18n.getMessage('popupTweeterContent', [
          '@' + Config.twitterAccount,
          '' + stats.totalBanner,
          '' + Number(saved.time).toFixed(1),
          saved.unit,
        ])
      )
    );
  }

  // Listen to update events
  window.background.addListener('storage.set', data => {
    if (!(data && 'stats' in data))
      return;

    update(data.stats);
  });

  // Initialize stats
  update(await window.background.sendMessage({
    type: 'storage.get',
    data: {
      what: 'stats',
    },
  }));
})();
