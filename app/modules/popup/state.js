/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");

(async () => {
  const tabId = window.tabId;

  const holiday = await window.background.sendMessage({
    type: 'holiday.get',
  });

  function update(state) {
    $("#status .sticker").removeClass(
      (i, c) => c.split(' ').filter(c => c.startsWith('state-')).join(' ')
    ).addClass(
      'state-' + state.name
    ).attr({
      'aria-label': state.sticker.alt,
    });
    $("#status .header").text(state.status);
    $("#status .desc").attr('data-content', state.desc);
  }

  // Listen to update events
  window.background.addListener('state.set', data => {
    if (tabId !== data.tabId)
      return;

    update(data.newState);
  });

  // Initialize state
  update(await window.background.sendMessage({
    type: 'state.get',
    data: tabId,
  }));

  // Setup holiday
  if (holiday) {
    $("#status .sticker").addClass(
      'holiday-' + holiday.class
    ).find('.tooltip').attr({
      'data-content': holiday.message,
    });
  }
})();
