/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const $ = require("jquery");
const { FiltersEngine } = require('@cliqz/adblocker');
const Config = require("./config.autoload");
const Storage = require("./storage.js");

// Register Rules at window level for options/popup page to access it
window.Rules = {
  // Lists of default values
  _lists : Config.rulesLists,
  _blockerLists: Config.rulesBlockerLists,

  // Cached data
  _listsData: {},
  _enginesData: {},

  _updateFrequency: Config.rulesUpdateFrequency,

  // _groupSelector: null,

  getData: async function({force = false} = {}) {
    // Get options
    const options = await Storage.getOptions();

    // Retrieve rule lists
    const {lists, blockerLists} = await Rules.getLists({
      force,
      options,
    });

    // Retrieve data
    let [rules, blockerEngines] = (await Promise.all([
      Promise.all(
        lists.map(async url => {
          return [url, await Rules.getList({url, force, options})];
        })
      ),
      Promise.all(
        blockerLists.map(async url => {
          return [url, await Rules.getBlockerList({url, force, options})];
        })
      ),
    ])).map(Object.fromEntries);

    // Return results
    return {
      lists,
      rules,

      blockerLists,
      blockerEngines,
    };
  },

  getDefaultLists: function() {
    return {
      lists: Rules._lists,
      blockerLists: Rules._blockerLists,
    };
  },

  getLists: function({force = false, options}) {
    // Setup current lists
    let lists = [];
    if (options.defaultLists in Rules._lists) {
      // Create copy of array
      lists = Rules._lists[options.defaultLists].slice();
    }
    lists.push(...options.customLists);

    // Setup current blocking lists
    let blockerLists = [];
    if (options.blocker) {
      if (options.defaultBlockerLists in Rules._blockerLists) {
        // Create copy of array
        blockerLists = Rules._blockerLists[options.defaultBlockerLists].slice();
      }
      blockerLists.push(...options.customBlockerLists);
    }

    return {
      lists: lists,
      blockerLists: blockerLists,
    };
  },

  _fetch: async function({url, lastModified = 0, dataType = null}) {
    let headers = {};
    if (lastModified) {
      headers = { "If-Modified-Since": (new Date(lastModified)).toUTCString() };
    }

    let data, status, xhr;
    try {
      [data, status, xhr] = await $.ajax({
        url,
        headers,
        dataType,
      }).then((d, s, x) => [d, s, x]);
      const header = xhr.getResponseHeader("Last-Modified");
      lastModified = header !== null ? Date.parse(header) : 0;
    } catch (error) {
      return {error};
    }

    switch (status) {
      case 'success':
        return {
          lastModified,
          data,
        };

      case 'notmodified':
        return {
          lastModified,
        };
      default:
        return {};
    }
  },

  _get: async function(url, force, cache, fetcher) {
    // Force delay so that data will be truly asynchronous
    // and will not start an infinite loop or run multiple times
    await new Promise(r => setTimeout(r, 1));

    if (force) {
      delete cache[url];
    }

    // Check if old info are available/outdated
    const oldData = cache[url];
    if (oldData) {
      const nextUpdate = oldData.metadata.lastUpdated + Rules._updateFrequency;
      if (nextUpdate > Date.now()) {
        return oldData;
      }
    }

    // Return new data once retrieved and cached for next time
    cache[url] = await fetcher(url, oldData);
    return cache[url];
  },

  getListInfo: async function(data) {
    const list = await Rules.getList(data);

    return {
      url: list.metadata.url,
      name: list.metadata.name || list.metadata.url,
      version: list.metadata.version,
      count: Object.keys(list).length - 1,
    };
  },

  getList: async function({url, force = false, options = null}) {
    return await Rules._get(url, force, Rules._listsData, async (url, oldData) => {
      const {data, lastModified, error} = await Rules._fetch({
        url,
        lastModified: oldData ? oldData.metadata.lastModified : 0,
        dataType: 'json'
      });
      if (error) {
        console.error(error);
        return oldData;
      }
      if (!data) {
        oldData.metadata.lastModified = lastModified;
        oldData.metadata.lastUpdated = Date.now();
        return oldData;
      }

      // Retrieve options if needed
      if (!options)
        options = await Storage.getOptions();

      // Prepare data
      if (!data.metadata || typeof data.metadata !== 'object') {
        data.metadata = {};
      }
      data.metadata.url = url;
      data.metadata.lastModified = lastModified;
      data.metadata.lastUpdated = Date.now();
      data.metadata.trusted = !options.customLists.includes(url);

      // Dispatch event
      await window.background.sendMessage({
        type: 'rules.getList',
        data,
      });

      return data;
    });
  },

  getBlockerListInfo: async function(data) {
    const engine = await Rules.getBlockerList(data);

    return {
      url: engine.metadata.url,
      name: engine.metadata.title || engine.metadata.url,
      version: engine.metadata.version,
      count: Object.values(engine.cosmetics).reduce(
        (acc, v) => acc + (v && v.numberOfFilters || 0), 0
      ),
    };
  },

  getBlockerList: async function({url, force = false, options = null}) {
    return await Rules._get(url, force, Rules._enginesData, async (url, oldData) => {
      const {data, lastModified, error} = await Rules._fetch({
        url,
        lastModified: oldData ? oldData.metadata.lastModified : 0,
      });
      if (error) {
        console.error(error);
        return oldData;
      }
      if (!data) {
        oldData.metadata.lastModified = lastModified;
        oldData.metadata.lastUpdated = Date.now();
        return oldData;
      }

      // Retrieve options if needed
      if (!options)
        options = await Storage.getOptions();

      // Prepare metadata
      const comments = data.split('\n').filter(
        l => l.startsWith('!')
      );
      const metadata = Object.fromEntries(
        comments.map(
          c => c.match(/! ([A-Za-z ]+): (.*)$/)
        ).map(
          m => m ? [m[1].toLowerCase().replaceAll(' ', '-'), m[2]] : null
        ).filter(
          e => !!e
        )
      );
      metadata.url = url;
      metadata.lastModified = lastModified;
      metadata.lastUpdated = Date.now();
      metadata.trusted = !options.customBlockerLists.includes(url);

      // Prepare engine
      let engine;
      try {
        engine = await FiltersEngine.parse(data);
      } catch (err) {
        console.error(err);
        return oldData;
      }

      engine.metadata = metadata;

      // Dispatch event
      window.background.sendMessage({
        type: 'rules.getBlockerList',
        data: engine,
      });

      return engine;
    });
  },
};

// // Try to find usable group selector
// (function () {
//   // Prepare test element
//   const div = document.createElement('div');
//   div.innerHTML = '<h1></h1><h2></h2><h3></h3><h4></h4>';

//   // Test each possible selector
//   const keywords = ['is', 'matches', 'any'];
//   const prefixes = ['', '-', '-ms-', '-moz-', '-o-', '-webkit-'];
//   for (let i = 0; i < keywords.length; i++) {
//     for (let j = 0; j < prefixes.length; j++) {
//       const selector = ':' + prefixes[j] + keywords[i];
//       try {
//         // TODO : test on more complex selector, maybe
//         const selected = div.querySelectorAll(selector + '(h1, h2, h3, h4)');
//         if (selected.length === 4) {
//           console.log(selector);
//           Rules._groupSelector = selector;
//           return;
//         }
//       } catch {}
//     }
//   }
// })();

module.exports = Rules;
