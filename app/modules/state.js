/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
window.State = {
  _data: {},
  _default: null,
  _definitions: {},
  _transitions: {},

  define: function(name, state, transitionFn = null) {
    state.name = name;

    // Default state is special
    if (name === 'default') {
      State._default = state;
    }

    // Save state definition
    State._definitions[name] = state;

    // Create a 2 dimension array of before/after state transitions
    // By default, missing values will be filled with new state
    const states = Object.keys(State._definitions);
    transitionFn = transitionFn ? transitionFn : (b, a, old) => (old || name);
    for (const i in states) {
      const before = states[i];
      if (!State._transitions[before])
        State._transitions[before] = {};
      for (const j in states) {
        const after = states[j];
        const value = transitionFn(
          before,
          after,
          State._transitions[before][after]
        );
        if (!value) {
          throw 'Transition from "' + before + '" to "' + after + '" is missing';
        }
        if (!(value in State._definitions)) {
          throw 'Transition from "' + before + '" to "' + after + '" leads to unknown "' + value + '" state';
        }
        State._transitions[before][after] = value;
      }
    }

    return state;
  },

  get: function(tabId) {
    if (!(tabId in State._data)) {
      State._data[tabId] = JSON.parse(JSON.stringify(State._default));
    }
    return State._data[tabId];
  },

  getAll: function() {
    return State._data;
  },

  set: async function(tabId, state = 'default', metadata = null) {
    const oldState = State.get(tabId);

    let newState = state;
    if (typeof state === 'string')
      newState = JSON.parse(JSON.stringify(State._definitions[state]));
    if (!(newState && newState.name in State._definitions))
      return oldState;
    if (metadata)
      newState.metadata = metadata;

    State._data[tabId] = newState;

    // Dispatch event
    await window.background.sendMessage({
      type: 'state.set',
      data: {
        tabId,
        oldState,
        newState,
      },
    });

    return state;
  },

  update: async function(tabId, state = 'default', metadata = null) {
    const currentState = State.get(tabId);

    if (typeof state === 'string')
      state = State._definitions[state];
    if (!(state && state.name in State._definitions))
      return currentState; // Unknown state

    // Get new state name
    const newStateName = State._transitions[currentState.name][state.name];

    // Don't update state if nothing changed
    if (currentState.name === newStateName && !metadata)
      return currentState;

    // Get new state
    const newState = JSON.parse(JSON.stringify(State._definitions[newStateName]));
    let newMetadata;
    if (newState.resetMetadata)
      newMetadata = metadata;
    else
      newMetadata = {...currentState.metadata, ...metadata};

    return await State.set(tabId, newState, newMetadata);
  },

  remove: async function(tabId) {
    const oldState = State.get(tabId);

    // Dispatch event
    await window.background.sendMessage({
      type: 'state.remove',
      data: {
        tabId,
        oldState,
      },
    });

    delete State._data[tabId];
  },

  replace: async function(newTabId, oldTabId) {
    const oldState = State.get(oldTabId);
    await State.set(newTabId, oldState);
    await State.remove(oldTabId);
  },
};

module.exports = State;
