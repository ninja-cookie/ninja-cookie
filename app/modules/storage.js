/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
const Config = require("./config.autoload");

// Register Storage at window level for options/popup page to access it
window.Storage = new Proxy({
  _default: Config.storageDefault,
  _data: null,

  getPremiumFields: function() {
    return Object.keys(Config.storageDefault.premiumOptions);
  },

  _getDefaultLists: function(options = ["beta", "rc"], fallback = "stable") {
    // Retrieve version to determine default list
    const manifest = browser.runtime.getManifest();
    const version = manifest.version_name || manifest.version;
    const branch = version.split("-")[1];

    return options.includes(branch) ? branch : fallback;
  },

  _update: function(target, source) {
    if (typeof source !== "object" || !source) {
      return target;
    }

    for (let key in target) {
      if (!(key in source)) {
        continue;
      }
      if (typeof source[key] !== typeof target[key]) {
        continue;
      }
      if (typeof source[key] === "object" && !Array.isArray(source[key])) {
        Storage._update(target[key], source[key]);
      } else {
        target[key] = source[key];
      }
    }

    return target;
  },

  _get: async function(target, {force = false} = {}) {
    if (force || !Storage._data) {
      Storage._data = (async () => {
        // Force delay so that data will be truly asynchronous
        // and will not start an infinite loop or run multiple times
        await new Promise(r => setTimeout(r, 1));

        // Clone default value
        const data = JSON.parse(JSON.stringify(Storage._default));

        // Retrieve default list
        data.options.defaultLists = Storage._getDefaultLists();
        data.options.defaultBlockerLists = Storage._getDefaultLists([]);

        // Retrieve stored information
        const store = await browser.storage.local.get(null);
        for (let key in store) {
          if (!Object.keys(data).includes(key)) {
            // Cleanup unknown keys
            await browser.storage.local.remove(key);
            delete store[key];
          }
        }

        // Update store with default values
        return Storage._update(data, store);
      })();
    }

    if (target) {
      return (await Storage._data)[target];
    } else {
      return await Storage._data;
    }
  },

  _set: async function(target, {force = false, data}) {
    if (target) {
      data = Object.fromEntries([[target, data]]);
    }

    // Retrieve original data
    const oldData = await Storage._get(null, {force});

    Storage._data = (async () => {
      // Force delay so that data will be truly asynchronous
      // and will not start an infinite loop or run multiple times
      await new Promise(r => setTimeout(r, 1));

      // Update stored data
      const newData = Storage._update(oldData, data);
      await browser.storage.local.set(newData);

      return newData;
    })();

    // Dispatch event
    await window.background.sendMessage({type: 'storage.set', data});

    if (target) {
      return (await Storage._data)[target];
    } else {
      return await Storage._data;
    }
  },

  _reset: async function(target, {keys = []}) {
    let data;
    if (target) {
      data = Storage._default[target];
    } else {
      data = Storage._default;
    }
    data = Object.fromEntries(
      Object.entries(data).filter(
        ([k, v]) => keys.includes(k)
      )
    );
    return await Storage._set(target, {data});
  },
}, {
  get: function(obj, property) {
    if (property in obj)
      return obj[property];

    const methods = ['get', 'set', 'reset'];
    for (let idx in methods) {
      const method = methods[idx];
      if (property.startsWith(method)) {
        const len = method.length;
        const target =
          property.slice(len, len + 1).toLowerCase() + property.slice(len + 1);

        return function() {
          return obj['_' + method].apply(this, [target, ...arguments]);
        };
      }
    }

    return obj[property];
  }
});

module.exports = Storage;
