# Table of contents
1. [Introduction](#introduction)
2. [Cookie banners](#cookie-banners)
    1. [Handling cookie banners](#handling)
    2. [Anatomy of a rule](#anatomy)
3. [Writing your own rules](#writing-rules)

## Introduction <a name="introduction"></a>

Ninja Cookie is a webextension supported on all major browsers:
- [Chrome (and Chromium-based browsers)](https://chrome.google.com/webstore/detail/ninja-cookie/jifeafcpcjjgnlcnkffmeegehmnmkefl)
- [Edge](https://microsoftedge.microsoft.com/addons/detail/ninja-cookie/eaiglkmcbamilakcbmpgjfoemjhbonpe)
- [Firefox](https://addons.mozilla.org/firefox/addon/ninja-cookie/reviews/)
- [Opera](https://addons.opera.com/fr/extensions/details/ninja-cookie/)
- [Safari (beta)](https://apps.apple.com/app/ninja-cookie/id1535219336)

Its goal is to handle cookie banners for the user. Ideally, the cookie banners would be hidden and the user would browse the internet without being disturbed by them.

## Cookie banners <a name="cookie-banners"></a>

In order to do so, we first need to analyse what types of cookie banners exists. We have categorized them in three groups:

- CMP (Consent Management Platform);
- Consent Tools;
- Others;

A lot of websites are using CMPs, an external company handling user consent for them. These CMPs are offering a set of parameters (hide/display "Deny All" buttons, load tracking script before/after consent, etc.), and generate a cookie banner automatically tailored for their customers websites.

Then, there are "Consent Tools". Is usually is a piece of code to include in one's website that will display a cookie banner. The users consent may or may not be recorded on the client/server side, and the same types of parameters are offered. There are quite a few more "Consent Tools" than there are CMPs.

Finally, any other cookie banner is specific to a website or a group of websites (eg. Ford is using the same cookie banner on all of theirs websites). These are problematic because of their volume. Listing them all is near impossible.


### Handling cookie banners <a name="handling"></a>

Given that a lot of websites are using the same CMPs/"Consent Tools", a simple approach is to create a set of "rules" that will take care of each of them. First we will detect if there is a known CMPs/"Consent Tools" on the visited webpage, then if any action is required and finally we will hide it and set it up in the background.

To make this process as simple as possible, the Ninja Cookie team created a rule syntax that will be detailed in this documentation.

### Anatomy of a rule <a name="anatomy"></a>

A rule is written un JSON. A simple example could look like that:

```json
{
  "metadata": {
    "name": "Cookiebot",
    "website": "https://www.cookiebot.com/",
    "iab": "cookiebot.mgr.consensu.org"
  },

  "match": [
    { "type": "check", "selector": "#CybotCookiebotDialog" }
  ],
  "action": [
    { "type": "hide" },
    { "type": "remove", "selector": "#CybotCookiebotDialogBodyUnderlay", "optional": true },
    { "type": "sleep" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonPreferences" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonStatistics" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonMarketing" },
    { "type": "sleep" },
    { "type": "click", "selector": "#CybotCookiebotDialogBodyLevelButtonAccept" }
  ]
}
```

The first block is a set of metadata describing the rule. Metadata are optionals. In this example, the rule is taking care of a CMP called "Cookiebot". It is a [IAB-compliant CMP](https://iabeurope.eu/cmp-list/) and its IAB subdomain is listed.

The first rule step is to match the CMP. It is done by checking if a specific CSS selector is available at some point on the webpage. A match can consist of multiple {@link Action actions}.

The second step is to check if an action is needed. It is optional and not used in this case. It would written as follow:

```json
{
  ...
  "required": [
    { "type": "check", "selector": ".my-required-selector" }
  ],
  ...
}
```

If any of the required {@link Action action} fails, it is considered that no action is required.

The third step is the actual action. It will first {@link HideAction hide} the latest selector element (previously selected during the `match` step), {@link RemoveAction remove} an overlay (if it exists), {@link SleepAction sleep} for some time, etc.

If any of these {@link Action actions} fails (unless marked as optional), the rule will be considered to have failed, and the status will be displayed as such.

## Writing your own rules <a name="writing-rules"></a>

It is possible to get involve in the Ninja's training by writing your own rules. Follow the tutorials to get started and experiment:

- [Setup your testing evironment]{@tutorial testing-setup}
- [Write your first rule]{@tutorial first-rule}
- [write advanced rules]{@tutorial advanced-rule}
- [Check the good practices]{@tutorial good-practice}
- [Share your rules]{@tutorial share-rule}


