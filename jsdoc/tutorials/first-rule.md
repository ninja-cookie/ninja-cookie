You are now set to write your first rule.

## Find a cookie banner

The first step is to find a website with a cookie banner not handled by your current set of activated rules. For this tutorial, we will write a rule for https://about.gitlab.com/ cookie banner.

**WARNING: Don't forget to disable any other webextension that could hide/remove a cookie banner.**

Once found, you can disable all the Ninja Cookie default rules lists and only leave your custom list as explained in the [testing evironment setup tutorial]{@tutorial testing-setup}.

**NOTE: Working in incognito mode will avoid any side-effect. This can be a good idea to test your rule, but it is always useful to test it in a "real world" setting.**

## Writing the rule

### Step 1 - Matching a rule

The first step is to test whether a rule should be applied to a webpage. The matching can rely on a DOM element being present, a cookie being set, an URL matching a regex, etc. It can also rely on a combination of all these checks.

**WARNING: You need to keep in mind that this step of the rule is critical! It will run on every webpage and must be lightweight. Checking the URL first, then if a DOM element is avaible will be better than the opposite.**

Most rules are checking for a DOM element. For https://about.gitlab.com/, the cookie banner is contained in the ```div``` with ```id="CybotCookiebotDialog"```.

![Finding the DOM element containing the cookie banner](first-rule-1.png)

Matching this element will be done using a {@link CheckAction} and a [CSS selector](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors) for the DOM element with the expected ID as follow:

```json
{
  ...
  "match": [
    { "type": "check", "selector": "#CybotCookiebotDialog" }
  ],
  ...
}
```

### Step 2 - Is a rule required (optional)

Some webpages will still have a DOM element containing the cookie banner; it will just be hidden if the cookie banner has already been set. In this case, there are (at least) two options:

- [Check if the DOM element is visible](#visibility);
- [Check if some cookie has already been set](#storage).


<a name="visibility"></a>To check if a DOM element is visible, it is possible to use the [extended jQuery selectors](https://api.jquery.com/category/selectors/jquery-selector-extensions/) (Ninja Cookie uses jQuery to query DOM elements). These selectors are a bit slower and a bit more demanding on processing power. This is why we should first check if a DOM element is present first and then check if said element is visible or not.

```json
{
  ...
  "required": [
    { "type": "check", "selector": "#CybotCookiebotDialog:visible" }
  ],
  ...
}
```

**NOTE: Using the ```"options": "attributes"``` might be useful if the cookie banner is made visible at a later date.**

<a name="storage"></a>To check if a cookie as already been set, use a {@link StorageAction}. It can check if a cookie (or a local storage value) exists or not, if it's empty or not and it can match against a regex:

```json
{
  ...
  "required": [
    { "type": "cookie", "name": "euconsent", "missing": true }
  ],
  ...
}
```

This step can contain as many actions as needed. If any of the steps is failing, the rule will not be executed and it will be considered successful.

### Step 3 - Executing the rule

Once the rule has been matched and it has been determined that it is required to run it, the actual action will be executed.

In most cases, a rule will have the following steps:
 - Hide the cookie banner;
 - Open the advanced options;
 - Setup the parameters (deny all data usage);
 - Save the parameters.

A rule will look smething like this:

```json
{
  ...
  "action": [
    { "type": "hide" },
    { "type": "sleep" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonPreferences" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonStatistics" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonMarketing" },
    { "type": "sleep" },
    { "type": "click", "selector": "#CybotCookiebotDialogBodyLevelButtonAccept" }
  ]
}
```

The first action is a {@link HideAction} that will hide the cookie banner.

**NOTE: Selected DOM elements from previous actions will be forwarded to the next action. The DOM elements forwarded to the execution step will be the selected elements from the matching step.**

The second action is a {@link SleepAction} that will wait for 500ms. It gives some time to the cookie banner to finish up its initialization.

The next three actions are {@link CheckboxAction}s that will de-select each field in the cookie banner.

**NOTE: In some cases, de-selecting a checkbox is not enough since the webpage will only take ```click``` events into accounts. In that case, use a {@link MouseEventAction} to simulate a mouse click.**

Then, another 500ms sleep leaves enough time to the cookie banner to handle the last events correctly.

Finally, a {@link MouseEventAction} will simulate a click on the 'OK' button to finalize the cookie banner setup.

### Setup 4 - Wrap up

Each rule has metadata associated to it. They are optionals, but they provide some information about the cookie banner. For a CMP, it will be a name, a company name, a website, a IAB subdomain, etc. For a consent tool, it will be a website, a GitHub repository, etc. There are no well-defined metadata keys are they are not used at the moment. In the future, they will be used to display extra information to the user.

In our example, the metadata will look something like this:

```json
{
  "metadata": {
    "name": "Cookiebot",
    "website": "https://www.cookiebot.com/",
    "iab": "cookiebot.mgr.consensu.org"
  },
  ...
}
```

The name and/or website of a CMP/consent-tool might not always be easy to find, depending on code obfuscation; just try your best! ;)

## The complete rule

```json
{
  "metadata": {
    "name": "Cookiebot",
    "website": "https://www.cookiebot.com/",
    "iab": "cookiebot.mgr.consensu.org"
  },

  "match": [
    { "type": "check", "selector": "#CybotCookiebotDialog" }
  ],
  "action": [
    { "type": "hide" },
    { "type": "remove", "selector": "#CybotCookiebotDialogBodyUnderlay", "optional": true },
    { "type": "sleep" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonPreferences" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonStatistics" },
    { "type": "checkbox", "selector": "#CybotCookiebotDialogBodyLevelButtonMarketing" },
    { "type": "sleep" },
    { "type": "click", "selector": "#CybotCookiebotDialogBodyLevelButtonAccept" }
  ]
}
```

This rule is pretty straightforward. In real life, there are a lot of weird cases. The best way to know how to write rules is to experiment with them. Get familiar with the different kind of {@link Action}s and try to imagine a case where you could use them.

It is possible to write rules that will act inside (iframes)[https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe], that will open a new window to take care of deeper settings in the background, that will execute actions conditionnally, etc. If you wish to go further, go check the [advanced rules tutorial]{@tutorial advanced-rule}.
