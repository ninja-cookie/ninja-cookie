At the moment it is not possible to share your rules with other users easily. You can put your rules files on a webserver of your choice (or a [GitHub Gist](https://gist.github.com/)) and give the URL to other Ninja Cookie users.

In the future, we would love to provide a simple sharing system and a hub for all existing rules lists.
