/*!
 * Copyright (c) 2020-present Théo Goudout. All rights reserved.
 *
 * This work is dual-licensed under Prorietary license and
 * GPL 3.0 (or any later version). You can choose between
 * one of them if you use this work.
 *
 * For details please refer to LICENSE file.
 */
"use strict";

/**
 * TypedArray.prototype.sort() polyfill
 *
 * On Safari 14, sorting a one-element TypedArray return 'undefined'
 */
[
  Int8Array,
  Uint8Array,
  Uint8ClampedArray,
  Int16Array,
  Uint16Array,
  Int32Array,
  Uint32Array
].forEach(type => {
  const sort = type.prototype.sort;
  type.prototype.sort = function() {
    if (this.length === 1) {
      return this;
    } else {
      return sort.apply(this, arguments);
    }
  }
});
