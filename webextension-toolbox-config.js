const webpack = require('webpack');

function addVersion(config, env) {
  // Find WebextensionPlugin
  let plugin = config.plugins.filter(p => p.constructor.name === 'WebextensionPlugin')[0];

  // Compute version name
  let versionName;
  if (process.env.CI_COMMIT_TAG) {
    versionName = process.env.CI_COMMIT_TAG;
  } else if (process.env.CI_COMMIT_BRANCH) {
    versionName = 'v' + plugin.manifestDefaults.version + '-' + process.env.CI_COMMIT_BRANCH;
  } else {
    try {
      const {gitDescribeSync} = require('git-describe');
      const gitInfo = gitDescribeSync({ longSemver: false });
      versionName = (!gitInfo.dirty && !gitInfo.distance) ? gitInfo.tag : gitInfo.raw;
    } catch {
      // Not a git repository -> Abort
      return config;
    }
  }
  if (env.vendor === "firefox") {
    plugin.manifestDefaults.version = versionName.substring(1);
  } else {
    plugin.manifestDefaults.version_name = versionName;
  }

  return config;
};

function addPolyfill(config, env) {
  const path = require('path');

  // Add extra polyfill
  let plugin = config.plugins.filter(p => p.constructor.name === 'ProvidePlugin')[0];
  plugin.definitions['window'] = path.normalize(__dirname + '/polyfill/.autoload');

  return config;
}

function addI18nSupport(config, env) {
  const CopyPlugin = require('copy-webpack-plugin');
  const gettextParser = require("gettext-parser");

  // Append copy plugin that will transform po file on the go
  config.plugins.unshift(new CopyPlugin([
    {
      from: '**/*.po',
      to: '[path][name].json',
      // Convert PO file to Webextension _locales file
      // Translator comment might be a description string or a JSON
      transform: (content, absoluteFrom) => {
        return JSON.stringify(Object.fromEntries(
          Object.entries(
            gettextParser.po.parse(content).translations['']
          ).filter(e => !!e[0]).map(([key, value]) => {
            let metadata = {};
            if (value.comments.extracted) {
              metadata = value.comments.extracted.split('\n').reduce(
                (acc, line) => {
                  const firstSpace = line.indexOf(' ');
                  if (firstSpace === -1) return acc;

                  const tag = line.substring(0, firstSpace);
                  const content = line.substring(firstSpace + 1);
                  switch(tag) {
                    case '@desc':
                    case '@description':
                      // Limit message description length for App Store
                      acc['description'] = content.substring(0, 112);
                      break;
                    case '@param':
                    case '@params':
                    case '@parameter':
                    case '@parameters':
                    case '@placeholder':
                    case '@placeholders':
                      if (!('placeholders' in acc)) acc['placeholders'] = {};

                      const index = Object.keys(acc['placeholders']).length + 1;
                      const firstEqual = content.indexOf('=');
                      if (firstEqual !== -1) {
                        const name = content.substring(0, firstEqual);
                        const example = content.substring(firstEqual + 1);
                        acc['placeholders'][name] = {
                          content: '$' + index,
                          example,
                        };
                      } else {
                        const name = content;
                        acc['placeholders'][name] = {
                          content: '$' + index,
                        };
                      }
                      break;
                    default:
                      console.warn("Unknown tag", tag);
                      break;
                  }
                  return acc;
                }, {}
              );
            }
            return [ key, {
              // Empty default description for App Store
              description: "",
              message: value.msgstr[0],
              reference: value.comments.reference,
              ...metadata,
            }];
          })
        ));
      }
    }
  ]));

  return config;
}

function addAutoloadSupport(config, env) {
  // Append action auto-loader.
  config.module.rules.unshift({
    test: /\.(autoload)$/,
    use: [{
      loader: 'val-loader',
    }]
  });

  return config;
}

function addSassSupport(config, env) {
  const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');

  config.module.rules.push(
  // Append sass loader/compiler
  {
    test: /\.(scss)$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          url: true,
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          postcssOptions: {
            plugins: function () {
              return [
                require('precss'),
                require('autoprefixer')
              ];
            }
          }
        }
      },
      'sass-loader',
    ]
  },
  // Append SVG loader/minifier/inliner
  {
    test: /\.svg$/,
    use: [
      {
        loader: 'base64-inline-loader',
      },
      {
        loader: ImageMinimizerPlugin.loader,
        options: {
          severityError: 'warning', // Ignore errors on corrupted images
          minimizerOptions: {
            plugins: ['svgo'],
          },
        },
      },
    ],
  });

  return config;
};

// function addImageOptimizer(config, env) {
//   const CopyPlugin = require('copy-webpack-plugin');
//   const OptiPng = require('optipng');

//   // Append copy plugin that will transform po file on the go
//   config.plugins.unshift(new CopyPlugin([
//     {
//       from: '**/*.png',
//       to: '[path][name].json',
//       // Convert PO file to Webextension _locales file
//       // Translator comment might be a description string or a JSON
//       transform: (content, absoluteFrom) => {
//         const opti = new OptiPng();
//         opti.end(content);
//         const result = opti.read();
//         console.log(opti, result);
//         return result;
//       }
//     }
//   ]));
//   const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');

//   return config;
// }

module.exports = {
  webpack: (config, env) => {
    config = addVersion(config, env);
    config = addPolyfill(config, env);
    config = addI18nSupport(config, env);
    config = addAutoloadSupport(config, env);
    config = addSassSupport(config, env);
    // config = addImageOptimizer(config, env);

    return config;
  },
  copyIgnore: [
    // Javascript files
    '**/*.js', '**/*.json',
    // I18n files
    '**/*.po', '**/*.pot', '**/*.mo',
    // Evaluated files
    '**/*.autoload',
    // SASS and inlined SVG files
    '**/*.scss', '**/*.svg',
    // PNG files are optimized
    // '**/*.png',
  ]
};
